MAKEFLAGS += --warn-undefined-variables
SHELL := /bin/bash -o pipefail

export GO111MODULE = on
export GOPRIVATE = go.aporeto.io,gitlab.com/t00f

.certificates:
	# Create a simple PKI
	# CA |> Public certificate
	#    |> System-CA -> Client cert / Service cert
	@if ! command -v tg >/dev/null 2>&1; then \
		echo "Cannot find tg. Please run go get go.aporeto.io/tg"; \
		exit 1; \
		fi
	@ mkdir -p .certificates
	@ tg cert --force --name ca --is-ca --out .certificates || true
	@ tg cert \
	--force \
	--name system-ca \
	--is-ca \
	--signing-cert .certificates/ca-cert.pem \
	--signing-cert-key .certificates/ca-key.pem --out .certificates || true
	@ tg cert \
	--force \
	--name public \
	--ip 0.0.0.0 \
	--ip 127.0.0.1 \
	--auth-server \
	--signing-cert .certificates/ca-cert.pem \
	--signing-cert-key .certificates/ca-key.pem --out .certificates || true
	@ tg cert \
	--force \
	--name service \
	--ip 127.0.0.1 \
	--dns pizza \
	--dns order \
	--auth-server \
	--signing-cert .certificates/system-ca-cert.pem \
	--signing-cert-key .certificates/system-ca-key.pem --out .certificates || true
	@ tg cert \
	--force \
	--name client \
	--auth-client \
	--signing-cert .certificates/system-ca-cert.pem \
	--signing-cert-key .certificates/system-ca-key.pem --out .certificates || true

certificates: .certificates

lint:
	golangci-lint run \
		--timeout=5m \
		--disable-all \
		--exclude-use-default=false \
		--enable=errcheck \
		--enable=goimports \
		--enable=ineffassign \
		--enable=golint \
		--enable=unused \
		--enable=structcheck \
		--enable=staticcheck \
		--enable=varcheck \
		--enable=deadcode \
		--enable=unconvert \
		--enable=misspell \
		--enable=prealloc \
		--enable=nakedret \
		--enable=typecheck \
		--enable=unparam \
		--enable=gosimple \
		./...

build: lint
	go build

build_linux: lint
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build

container: build_linux
	@mv backend-demo docker/
	@cd docker && docker build --no-cache -t backend-demo:latest .

run_pizza:
	# Starting pizza service
	@./backend-demo pizza --listen :8001 2>&1 | sed 's/^/[PIZZA]: /'

run_order:
	# Starting order service
	@./backend-demo order --listen :8002 2>&1 | sed 's/^/[ORDER]: /'

run_gateway:
	# Starting gateway service
	@./backend-demo gateway 2>&1 | sed 's/^/[GATEWAY]: /'

run_mongo:
	# Starting mongo
	@if ! command -v mongod >/dev/null; then echo "Cannot find mongod. Please install mongo"; exit 1; fi
	@mkdir -p .data/mongo
	@mongod --quiet -dbpath .data/mongo --bind_ip 0.0.0.0 2>&1 | sed 's/^/[MONGO]: /'

run_nats:
	# Starting nats
	@if ! command -v nats-server >/dev/null; then echo "Cannot find nats-server. Please install nats-server"; exit 1; fi
	@nats-server 2>&1 | sed 's/^/[NATS]: /'

_run: run_nats run_mongo run_gateway run_pizza run_order
	@tail -f /dev/null && read -r

run: certificates build
	@$(MAKE) _run -j

compose: certificates container
	# make sure docker is runing
	@if ! docker info >/dev/null 2>&1 ; then echo "Docker is not running" ; exit 1 ; fi
	# Starting the demo as docker-compose
	@docker-compose up -d

decompose:
	# Stoping the demo
	@docker-compose down

clean: decompose
	@ rm -rf .certificates
	@ rm -rf .data
