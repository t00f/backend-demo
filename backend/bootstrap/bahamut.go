package bootstrap

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"net"
	"os"

	"gitlab.com/t00f/backend-demo/models"
	"go.aporeto.io/bahamut"
	"go.aporeto.io/bahamut/gateway/upstreamer/push"
	"go.aporeto.io/elemental"
	"go.aporeto.io/tg/tglib"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// MakeBahamutServer boostrap a bahamut server given a configuration
func MakeBahamutServer(name string, version string, configuration ServiceConf) (context.Context, bahamut.Server, error) {

	// Initialize a context
	ctx, cancel := context.WithCancel(context.Background())

	// Initialize logger
	logger, _ := zap.NewDevelopment()
	zap.ReplaceGlobals(logger.WithOptions(zap.IncreaseLevel(zapcore.DebugLevel)))

	// handle gravcefull stop
	bahamut.InstallSIGINTHandler(cancel)

	// Initialize tls
	serverCertificates, serverKey, err := tglib.ReadCertificatePEMs(configuration.ServiceCertPath, configuration.ServiceCertKeyPath, configuration.ServiceCertKeyPass)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to read server tls configuration: %s", err)
	}

	serverTLSCert, err := tglib.ToTLSCertificates(serverCertificates, serverKey)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to convert server cert to tls.Certificate: %s", err)
	}

	publicCABytes, err := ioutil.ReadFile(configuration.CACertPath)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to read CACertPath: %s", err)
	}

	systemCABytes, err := ioutil.ReadFile(configuration.SystemCACertPath)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to read CACertPath: %s", err)
	}

	clientCAPool := x509.NewCertPool()
	clientCAPool.AppendCertsFromPEM(publicCABytes)
	clientCAPool.AppendCertsFromPEM(systemCABytes)

	serverTLSConfig := &tls.Config{
		Certificates: []tls.Certificate{serverTLSCert},
		ClientAuth:   tls.VerifyClientCertIfGiven,
		ClientCAs:    clientCAPool,
	}

	// Connect to nats
	pubsub := bahamut.NewNATSPubSubClient(configuration.NATS)
	if err := pubsub.Connect(ctx); err != nil {
		return nil, nil, fmt.Errorf("unable to connect to nats: %w", err)
	}

	// Create the gateway notifier
	pn := push.NewNotifier(pubsub, "gw.services", name, getNotifierEndpoint(configuration.Listen))

	// Initialize the server
	return ctx, bahamut.New(
		bahamut.OptServiceInfo(name, version, nil),
		bahamut.OptModel(map[int]elemental.ModelManager{0: models.Manager(), 1: models.Manager()}),
		bahamut.OptTLS(serverTLSConfig.Certificates, nil),
		bahamut.OptMTLS(serverTLSConfig.ClientCAs, tls.RequireAndVerifyClientCert),
		bahamut.OptRestServer(configuration.Listen),
		bahamut.OptPostStartHook(pn.MakeStartHook(ctx)),
		bahamut.OptPreStopHook(pn.MakeStopHook()),
	), nil
}

// getNotifierEndpoint report the ip that the service is actually using
func getNotifierEndpoint(listenAddress string) string {

	_, port, err := net.SplitHostPort(listenAddress)
	if err != nil {
		zap.L().Fatal("Unable to parse listen address", zap.Error(err))
	}

	host, err := os.Hostname()
	if err != nil {
		zap.L().Fatal("Unable to retrieve hostname", zap.Error(err))
	}

	addrs, err := net.LookupHost(host)
	if err != nil {
		zap.L().Fatal("Unable to resolve hostname", zap.Error(err))
	}

	if len(addrs) == 0 {
		zap.L().Fatal("Unable to find any IP in resolved hostname")
	}

	var endpoint string
	for _, addr := range addrs {
		ip := net.ParseIP(addr)
		if len(ip.To4()) == net.IPv4len {
			endpoint = addr
			break
		}
	}

	if endpoint == "" {
		endpoint = addrs[0]
	}

	return fmt.Sprintf("%s:%s", endpoint, port)
}
