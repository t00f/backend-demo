package bootstrap

// MongoConf represent the mongodb configuration
type MongoConf struct {
	MongoURL    string `mapstructure:"mongo-url"    desc:"Mongo database address"   default:"mongodb://127.0.0.1:27017"`
	MongoDBName string `mapstructure:"mongo-db"     desc:"Mongo database name"      default:"changeme"`
}

// ServiceConf represent a generic service configuration
type ServiceConf struct {
	Listen             string `mapstructure:"listen"                desc:"Listen address"                           default:":443"`
	CACertPath         string `mapstructure:"ca-cert-path"          desc:"Path to the certificate authority"        default:".certificates/ca-cert.pem"`
	SystemCACertPath   string `mapstructure:"system-ca-cert-path"   desc:"Path to the certificate authority"        default:".certificates/system-ca-cert.pem"`
	ServiceCertPath    string `mapstructure:"service-cert-path"     desc:"Path to the service certificate"          default:".certificates/service-cert.pem"`
	ServiceCertKeyPath string `mapstructure:"service-cert-key-path" desc:"Path to the service certificate key"      default:".certificates/service-key.pem"`
	ServiceCertKeyPass string `mapstructure:"service-cert-pass"     desc:"Password for the service certificate key" default:""`
	NATS               string `mapstructure:"nats-url"              desc:"URL of nats"                              default:"nats://127.0.0.1:4222"`
}
