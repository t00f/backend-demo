package gateway

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"syscall"

	"go.aporeto.io/addedeffect/lombric"
	"go.aporeto.io/bahamut"
	"go.aporeto.io/bahamut/gateway"
	"go.aporeto.io/bahamut/gateway/upstreamer/push"
	"go.aporeto.io/tg/tglib"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type configuration struct {
	CACertPath        string `mapstructure:"ca-cert-path" desc:"Path to the certificate authority" default:".certificates/ca-cert.pem"`
	CertPath          string `mapstructure:"cert-path" desc:"Path to the certificate of the gateway" default:".certificates/public-cert.pem"`
	CertKeyPath       string `mapstructure:"cert-key-path" desc:"Path to the certificate key of the gateway" default:".certificates/public-key.pem"`
	CertKeyPass       string `mapstructure:"cert-pass" desc:"Password for the certificate key" default:""`
	SystemCACertPath  string `mapstructure:"system-ca-cert-path" desc:"Path to the system certificate authority" default:".certificates/system-ca-cert.pem"`
	ClientCertPath    string `mapstructure:"client-cert-path" desc:"Path to the client certificate of the gateway" default:".certificates/client-cert.pem"`
	ClientCertKeyPath string `mapstructure:"client-cert-key-path" desc:"Path to the client certificate key of the gateway" default:".certificates/client-key.pem"`
	ClientCertKeyPass string `mapstructure:"client-cert-pass" desc:"Password for the client certificate key" default:""`
	Listen            string `mapstructure:"listen" desc:"Listen address"  default:":3443"`
	NATS              string `mapstructure:"nats-url" desc:"URL of nats"   default:"nats://127.0.0.1:4222"`
}

func (c *configuration) Prefix() string { return "gateway" }

// Start the gateway
func Start() {

	// Initialize a context
	ctx, cancel := context.WithCancel(context.Background())

	// Initialize configuration
	configuration := &configuration{}
	lombric.Initialize(configuration)

	// Initialize logger
	logger, _ := zap.NewDevelopment()
	zap.ReplaceGlobals(logger.WithOptions(zap.IncreaseLevel(zapcore.DebugLevel)))

	// Prepare TLS Config
	serverTLSConfig, clientTLSConfig, err := makeTLSConfigs(configuration)
	if err != nil {
		zap.L().Fatal("Unable to build tls config", zap.Error(err))
	}

	// Connect to nats
	pubsub := bahamut.NewNATSPubSubClient(configuration.NATS)

	if err := pubsub.Connect(ctx); err != nil {
		panic("unable to connect to nats" + err.Error())
	}

	// Initialize the upstreamer
	upstreamer := push.NewUpstreamer(pubsub, "gw.services", "gw.gateways")
	readyToStart, readyToStop := upstreamer.Start(ctx)
	<-readyToStart

	// Initialize and start the gateway
	gw, err := gateway.New(
		configuration.Listen,
		upstreamer,
		gateway.OptionServerTLSConfig(serverTLSConfig),
		gateway.OptionUpstreamTLSConfig(clientTLSConfig),
		gateway.OptionAllowedCORSOrigin("*"),
	)
	if err != nil {
		zap.L().Fatal("unable to create new gateway", zap.Error(err))
	}

	gw.Start()

	zap.L().Info("Gateway started", zap.String("listen", configuration.Listen))

	// Handle proper shutdown
	c := make(chan os.Signal, 1)
	signal.Reset(syscall.SIGINT, syscall.SIGTERM)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
	<-c

	cancel()
	gw.Stop()
	readyToStop.Wait()
}

func makeTLSConfigs(configuration *configuration) (serverTLSConfig *tls.Config, clientTLSConfig *tls.Config, err error) {

	serverCertificates, serverKey, err := tglib.ReadCertificatePEMs(configuration.CertPath, configuration.CertKeyPath, configuration.CertKeyPass)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to read server tls configuration: %s", err)
	}

	serverTLSCert, err := tglib.ToTLSCertificates(serverCertificates, serverKey)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to convert server cert to tls.Certificate: %s", err)
	}

	clientCertificates, clientKey, err := tglib.ReadCertificatePEMs(configuration.ClientCertPath, configuration.ClientCertKeyPath, configuration.ClientCertKeyPass)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to read server tls configuration: %s", err)
	}

	clientTLSCert, err := tglib.ToTLSCertificates(clientCertificates, clientKey)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to convert server cert to tls.Certificate: %s", err)
	}

	caBytes, err := ioutil.ReadFile(configuration.CACertPath)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to read CACertPath: %s", err)
	}

	systemCABytes, err := ioutil.ReadFile(configuration.SystemCACertPath)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to read SystemCACertPath: %s", err)
	}

	clientCAPool := x509.NewCertPool()
	clientCAPool.AppendCertsFromPEM(systemCABytes)

	systemCAPool := x509.NewCertPool()
	systemCAPool.AppendCertsFromPEM(caBytes)
	systemCAPool.AppendCertsFromPEM(systemCABytes)

	serverTLSConfig = &tls.Config{
		Certificates: []tls.Certificate{serverTLSCert},
		ClientAuth:   tls.VerifyClientCertIfGiven,
		ClientCAs:    clientCAPool,
	}

	clientTLSConfig = &tls.Config{
		Certificates:       []tls.Certificate{clientTLSCert},
		RootCAs:            systemCAPool,
		InsecureSkipVerify: true, // Required because services are registering with their IP
	}

	return serverTLSConfig, clientTLSConfig, nil
}
