package processors

import (
	"fmt"
	"net/http"

	"gitlab.com/t00f/backend-demo/models"
	"go.aporeto.io/bahamut"
	"go.aporeto.io/elemental"
	"go.aporeto.io/manipulate"
	"go.uber.org/zap"
)

// OrderProcessor processes requests coming on /orders endpoint
type OrderProcessor struct {
	mongoManipulator manipulate.TransactionalManipulator
}

// NewOrderProcessor creates an instance of the Order processor.
func NewOrderProcessor(mongoManipulator manipulate.TransactionalManipulator) *OrderProcessor {

	return &OrderProcessor{
		mongoManipulator: mongoManipulator,
	}
}

// ProcessRetrieveMany implements the retrieve-many capability as a bahamut Processor.
func (p *OrderProcessor) ProcessRetrieveMany(bctx bahamut.Context) error {

	zap.L().Info("received request", zap.String("op", string(bctx.Request().Operation)))

	out := models.OrdersList{}
	if err := p.mongoManipulator.RetrieveMany(nil, &out); err != nil {
		return elemental.NewError("Error", fmt.Sprintf("unable to retrieve orders: %s", err.Error()), "order", http.StatusInternalServerError)
	}

	bctx.SetOutputData(out)

	return nil
}

// ProcessCreate implements the create capability as a bahamut Processor.
func (p *OrderProcessor) ProcessCreate(bctx bahamut.Context) error {

	order := bctx.InputData().(*models.Order)

	return p.mongoManipulator.Create(nil, order)
}

// ProcessUpdate implements the update capability as a bahamut Processor.
func (p *OrderProcessor) ProcessUpdate(bctx bahamut.Context) error {

	order := bctx.InputData().(*models.Order)
	order.ID = bctx.Request().ObjectID

	return p.mongoManipulator.Update(nil, order)
}

// ProcessDelete implements the delete capability as a bahamut Processor.
func (p *OrderProcessor) ProcessDelete(bctx bahamut.Context) error {

	order := models.NewOrder()
	order.ID = bctx.Request().ObjectID

	return p.mongoManipulator.Delete(nil, order)
}

// ProcessRetrieve implements the delete capability as a bahamut Processor.
func (p *OrderProcessor) ProcessRetrieve(bctx bahamut.Context) error {

	order := models.NewOrder()
	order.ID = bctx.Request().ObjectID

	if err := p.mongoManipulator.Retrieve(nil, order); err != nil {
		return nil
	}

	bctx.SetOutputData(order)
	return nil
}
