package order

import (
	"gitlab.com/t00f/backend-demo/backend/bootstrap"
	"gitlab.com/t00f/backend-demo/backend/order/internal/processors"
	"gitlab.com/t00f/backend-demo/models"
	"go.aporeto.io/addedeffect/lombric"
	"go.aporeto.io/bahamut"
	"go.aporeto.io/manipulate/manipmongo"
	"go.uber.org/zap"
)

type configuration struct {
	bootstrap.MongoConf   `mapstructure:",squash" override:"mongo-db=order"`
	bootstrap.ServiceConf `mapstructure:",squash"`
}

func (c *configuration) Prefix() string { return "order" }

// Start the order service.
func Start() {

	// Initialize configuration
	configuration := &configuration{}
	lombric.Initialize(configuration)

	ctx, server, err := bootstrap.MakeBahamutServer("order", "v1.0", configuration.ServiceConf)
	if err != nil {
		zap.L().Fatal("Unable to bootstrap bahamut server", zap.Error(err))
	}

	// Initialize mongodb
	mongoManipulator, err := manipmongo.New(configuration.MongoURL, configuration.MongoDBName)
	if err != nil {
		zap.L().Fatal("Unable to connect to mongo", zap.Error(err))
	}

	// Register the processor of orders for the Order resource.
	bahamut.RegisterProcessorOrDie(server, processors.NewOrderProcessor(mongoManipulator), models.OrderIdentity)

	// Run the server
	server.Run(ctx)

	<-ctx.Done()
}
