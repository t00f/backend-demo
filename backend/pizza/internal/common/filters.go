package common

import (
	"net/http"

	"go.aporeto.io/elemental"
)

// QueryFilters returns query filters
func QueryFilters(parameters elemental.Parameters) (*elemental.Filter, error) {

	filters := []*elemental.Filter{}

	for _, query := range parameters.Get("q").StringValues() {
		f, err := elemental.NewFilterFromString(query)
		if err != nil {
			return nil, elemental.NewError("Invalid filter", err.Error(), "common", http.StatusBadRequest)
		}

		filters = append(filters, f.Done())
	}

	if len(filters) == 0 {
		return nil, nil
	}

	return elemental.NewFilterComposer().And(filters...).Done(), nil
}
