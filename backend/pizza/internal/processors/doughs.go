package processors

import (
	"fmt"
	"net/http"

	"gitlab.com/t00f/backend-demo/backend/pizza/internal/common"
	"gitlab.com/t00f/backend-demo/models"
	"go.aporeto.io/bahamut"
	"go.aporeto.io/elemental"
	"go.aporeto.io/manipulate"
	"go.uber.org/zap"
)

// DoughsProcessor processes requests coming on /doughs endpoint
type DoughsProcessor struct {
	mongoManipulator manipulate.TransactionalManipulator
}

// NewDoughsProcessor creates an instance of the Dough processor.
func NewDoughsProcessor(mongoManipulator manipulate.TransactionalManipulator) *DoughsProcessor {

	return &DoughsProcessor{
		mongoManipulator: mongoManipulator,
	}
}

// ProcessRetrieveMany implements the retrieve-many capability as a bahamut Processor.
func (p *DoughsProcessor) ProcessRetrieveMany(bctx bahamut.Context) error {

	zap.L().Info("received request", zap.String("op", string(bctx.Request().Operation)))

	filter, err := common.QueryFilters(bctx.Request().Parameters)
	if err != nil {
		return err
	}

	opts := []manipulate.ContextOption{}
	if filter != nil {
		opts = append(opts, manipulate.ContextOptionFilter(filter))
	}

	mctx := manipulate.NewContext(bctx.Context(),
		opts...,
	)

	out := models.DoughsList{}
	if err := p.mongoManipulator.RetrieveMany(mctx, &out); err != nil {
		return elemental.NewError("Error", fmt.Sprintf("unable to retrieve doughs: %s", err.Error()), "dough", http.StatusInternalServerError)
	}

	bctx.SetOutputData(out)

	return nil
}

// ProcessCreate implements the create capability as a bahamut Processor.
func (p *DoughsProcessor) ProcessCreate(bctx bahamut.Context) error {

	dough := bctx.InputData().(*models.Dough)

	return p.mongoManipulator.Create(nil, dough)
}

// ProcessUpdate implements the update capability as a bahamut Processor.
func (p *DoughsProcessor) ProcessUpdate(bctx bahamut.Context) error {

	dough := bctx.InputData().(*models.Dough)
	dough.ID = bctx.Request().ObjectID

	return p.mongoManipulator.Update(nil, dough)
}

// ProcessDelete implements the delete capability as a bahamut Processor.
func (p *DoughsProcessor) ProcessDelete(bctx bahamut.Context) error {

	dough := models.NewDough()
	dough.ID = bctx.Request().ObjectID

	return p.mongoManipulator.Delete(nil, dough)
}

// ProcessRetrieve implements the delete capability as a bahamut Processor.
func (p *DoughsProcessor) ProcessRetrieve(bctx bahamut.Context) error {

	dough := models.NewDough()
	dough.ID = bctx.Request().ObjectID

	if err := p.mongoManipulator.Retrieve(nil, dough); err != nil {
		return nil
	}

	bctx.SetOutputData(dough)
	return nil
}
