package processors

import (
	"fmt"
	"net/http"

	"gitlab.com/t00f/backend-demo/models"
	"go.aporeto.io/bahamut"
	"go.aporeto.io/elemental"
	"go.aporeto.io/manipulate"
	"go.uber.org/zap"
)

// PizzaProcessor processes requests coming on /pizzas endpoint
type PizzaProcessor struct {
	mongoManipulator manipulate.TransactionalManipulator
}

// NewPizzaProcessor creates an instance of the Pizza processor.
func NewPizzaProcessor(mongoManipulator manipulate.TransactionalManipulator) *PizzaProcessor {

	return &PizzaProcessor{
		mongoManipulator: mongoManipulator,
	}
}

// ProcessRetrieveMany implements the retrieve-many capability as a bahamut Processor.
func (p *PizzaProcessor) ProcessRetrieveMany(bctx bahamut.Context) error {

	zap.L().Info("received request", zap.String("op", string(bctx.Request().Operation)))

	out := models.PizzasList{}
	if err := p.mongoManipulator.RetrieveMany(nil, &out); err != nil {
		return elemental.NewError("Error", fmt.Sprintf("unable to retrieve pizzas: %s", err.Error()), "pizza", http.StatusInternalServerError)
	}

	bctx.SetOutputData(out)

	return nil
}

// ProcessCreate implements the create capability as a bahamut Processor.
func (p *PizzaProcessor) ProcessCreate(bctx bahamut.Context) error {

	pizza := bctx.InputData().(*models.Pizza)

	return p.mongoManipulator.Create(nil, pizza)
}

// ProcessUpdate implements the update capability as a bahamut Processor.
func (p *PizzaProcessor) ProcessUpdate(bctx bahamut.Context) error {

	pizza := bctx.InputData().(*models.Pizza)
	pizza.ID = bctx.Request().ObjectID

	return p.mongoManipulator.Update(nil, pizza)
}

// ProcessDelete implements the delete capability as a bahamut Processor.
func (p *PizzaProcessor) ProcessDelete(bctx bahamut.Context) error {

	pizza := models.NewPizza()
	pizza.ID = bctx.Request().ObjectID

	return p.mongoManipulator.Delete(nil, pizza)
}

// ProcessRetrieve implements the delete capability as a bahamut Processor.
func (p *PizzaProcessor) ProcessRetrieve(bctx bahamut.Context) error {

	pizza := models.NewPizza()
	pizza.ID = bctx.Request().ObjectID

	if err := p.mongoManipulator.Retrieve(nil, pizza); err != nil {
		return nil
	}

	bctx.SetOutputData(pizza)
	return nil
}
