package processors

import (
	"fmt"
	"net/http"

	"gitlab.com/t00f/backend-demo/backend/pizza/internal/common"
	"gitlab.com/t00f/backend-demo/models"
	"go.aporeto.io/bahamut"
	"go.aporeto.io/elemental"
	"go.aporeto.io/manipulate"
	"go.uber.org/zap"
)

// SaucesProcessor processes requests coming on /sauces endpoint
type SaucesProcessor struct {
	mongoManipulator manipulate.TransactionalManipulator
}

// NewSaucesProcessor creates an instance of the Sauce processor.
func NewSaucesProcessor(mongoManipulator manipulate.TransactionalManipulator) *SaucesProcessor {

	return &SaucesProcessor{
		mongoManipulator: mongoManipulator,
	}
}

// ProcessRetrieveMany implements the retrieve-many capability as a bahamut Processor.
func (p *SaucesProcessor) ProcessRetrieveMany(bctx bahamut.Context) error {

	zap.L().Info("received request", zap.String("op", string(bctx.Request().Operation)))

	filter, err := common.QueryFilters(bctx.Request().Parameters)
	if err != nil {
		return err
	}

	opts := []manipulate.ContextOption{}
	if filter != nil {
		opts = append(opts, manipulate.ContextOptionFilter(filter))
	}

	mctx := manipulate.NewContext(bctx.Context(),
		opts...,
	)

	out := models.SaucesList{}
	if err := p.mongoManipulator.RetrieveMany(mctx, &out); err != nil {
		return elemental.NewError("Error", fmt.Sprintf("unable to retrieve sauces: %s", err.Error()), "sauce", http.StatusInternalServerError)
	}

	bctx.SetOutputData(out)

	return nil
}

// ProcessCreate implements the create capability as a bahamut Processor.
func (p *SaucesProcessor) ProcessCreate(bctx bahamut.Context) error {

	sauce := bctx.InputData().(*models.Sauce)

	return p.mongoManipulator.Create(nil, sauce)
}

// ProcessUpdate implements the update capability as a bahamut Processor.
func (p *SaucesProcessor) ProcessUpdate(bctx bahamut.Context) error {

	sauce := bctx.InputData().(*models.Sauce)
	sauce.ID = bctx.Request().ObjectID

	return p.mongoManipulator.Update(nil, sauce)
}

// ProcessDelete implements the delete capability as a bahamut Processor.
func (p *SaucesProcessor) ProcessDelete(bctx bahamut.Context) error {

	sauce := models.NewSauce()
	sauce.ID = bctx.Request().ObjectID

	return p.mongoManipulator.Delete(nil, sauce)
}

// ProcessRetrieve implements the delete capability as a bahamut Processor.
func (p *SaucesProcessor) ProcessRetrieve(bctx bahamut.Context) error {

	sauce := models.NewSauce()
	sauce.ID = bctx.Request().ObjectID

	if err := p.mongoManipulator.Retrieve(nil, sauce); err != nil {
		return nil
	}

	bctx.SetOutputData(sauce)
	return nil
}
