package processors

import (
	"fmt"
	"net/http"

	"gitlab.com/t00f/backend-demo/backend/pizza/internal/common"
	"gitlab.com/t00f/backend-demo/models"
	"go.aporeto.io/bahamut"
	"go.aporeto.io/elemental"
	"go.aporeto.io/manipulate"
	"go.uber.org/zap"
)

// ToppingsProcessor processes requests coming on /toppings endpoint
type ToppingsProcessor struct {
	mongoManipulator manipulate.TransactionalManipulator
}

// NewToppingsProcessor creates an instance of the Topping processor.
func NewToppingsProcessor(mongoManipulator manipulate.TransactionalManipulator) *ToppingsProcessor {

	return &ToppingsProcessor{
		mongoManipulator: mongoManipulator,
	}
}

// ProcessRetrieveMany implements the retrieve-many capability as a bahamut Processor.
func (p *ToppingsProcessor) ProcessRetrieveMany(bctx bahamut.Context) error {

	zap.L().Info("received request", zap.String("op", string(bctx.Request().Operation)))

	filter, err := common.QueryFilters(bctx.Request().Parameters)
	if err != nil {
		return err
	}

	opts := []manipulate.ContextOption{}
	if filter != nil {
		opts = append(opts, manipulate.ContextOptionFilter(filter))
	}

	mctx := manipulate.NewContext(bctx.Context(),
		opts...,
	)

	out := models.ToppingsList{}
	if err := p.mongoManipulator.RetrieveMany(mctx, &out); err != nil {
		return elemental.NewError("Error", fmt.Sprintf("unable to retrieve toppings: %s", err.Error()), "topping", http.StatusInternalServerError)
	}

	bctx.SetOutputData(out)

	return nil
}

// ProcessCreate implements the create capability as a bahamut Processor.
func (p *ToppingsProcessor) ProcessCreate(bctx bahamut.Context) error {

	topping := bctx.InputData().(*models.Topping)

	return p.mongoManipulator.Create(nil, topping)
}

// ProcessUpdate implements the update capability as a bahamut Processor.
func (p *ToppingsProcessor) ProcessUpdate(bctx bahamut.Context) error {

	topping := bctx.InputData().(*models.Topping)
	topping.ID = bctx.Request().ObjectID

	return p.mongoManipulator.Update(nil, topping)
}

// ProcessDelete implements the delete capability as a bahamut Processor.
func (p *ToppingsProcessor) ProcessDelete(bctx bahamut.Context) error {

	topping := models.NewTopping()
	topping.ID = bctx.Request().ObjectID

	return p.mongoManipulator.Delete(nil, topping)
}

// ProcessRetrieve implements the delete capability as a bahamut Processor.
func (p *ToppingsProcessor) ProcessRetrieve(bctx bahamut.Context) error {

	topping := models.NewTopping()
	topping.ID = bctx.Request().ObjectID

	if err := p.mongoManipulator.Retrieve(nil, topping); err != nil {
		return nil
	}

	bctx.SetOutputData(topping)
	return nil
}
