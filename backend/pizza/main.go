package pizza

import (
	"fmt"

	"gitlab.com/t00f/backend-demo/backend/bootstrap"
	"gitlab.com/t00f/backend-demo/backend/pizza/internal/processors"
	"gitlab.com/t00f/backend-demo/models"
	"go.aporeto.io/addedeffect/lombric"
	"go.aporeto.io/bahamut"
	"go.aporeto.io/manipulate/manipmongo"
	"go.uber.org/zap"
)

type configuration struct {
	bootstrap.MongoConf   `mapstructure:",squash" override:"mongo-db=pizza"`
	bootstrap.ServiceConf `mapstructure:",squash"`
}

func (c *configuration) Prefix() string { return "pizza" }

// Start the pizza service
func Start() {

	// Initialize configuration
	configuration := &configuration{}
	lombric.Initialize(configuration)

	ctx, server, err := bootstrap.MakeBahamutServer("pizza", "v1.0", configuration.ServiceConf)
	if err != nil {
		zap.L().Fatal("Unable to bootstrap bahamut server", zap.Error(err))
	}

	// Initialize mongodb
	mongoManipulator, err := manipmongo.New(configuration.MongoURL, configuration.MongoDBName)
	if err != nil {
		zap.L().Fatal("Unable to connect to mongo", zap.Error(err))
	}

	fmt.Println("coucou")

	// Register the processor of pizzas for the Pizza resource.
	bahamut.RegisterProcessorOrDie(server, processors.NewPizzaProcessor(mongoManipulator), models.PizzaIdentity)
	bahamut.RegisterProcessorOrDie(server, processors.NewDoughsProcessor(mongoManipulator), models.DoughIdentity)
	bahamut.RegisterProcessorOrDie(server, processors.NewSaucesProcessor(mongoManipulator), models.SauceIdentity)
	bahamut.RegisterProcessorOrDie(server, processors.NewToppingsProcessor(mongoManipulator), models.ToppingIdentity)

	// Run the server
	server.Run(ctx)

	<-ctx.Done()
}
