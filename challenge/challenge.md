
# Context

We plan to open a pizza factory where customer can build their own pizzas.
Before being able to build a pizzas, we have to manage doughs, sauces and toppings
that will be available to our customers.


## Model
The provided backend handles `GET`, `POST`, `PUT`, `DELETE` requests on the following resources:

* /doughs
* /sauces
* /toppings

> Note: All these models are defined in specification files (extension `.spec`) under `models/specs` folder.

See below the properties of each resource.

### Dough

``` yaml
attributes:
  - name: description
    description: The description of the dough.
    type: string
    exposed: true
    stored: true

  - name: name
    description: The name of the dough.
    type: string
    exposed: true
    stored: true

  - name: style
    description: The style of the dough.
    type: enum
    exposed: true
    stored: true
    allowed_choices:
    - NewYork
    - Napolitan
    - Sicilian
    - Cheesecrust
    default_value: NewYork
```

### Sauce

```yaml
attributes:
  - name: description
    description: The description of the sauce.
    type: string
    exposed: true
    stored: true

  - name: name
    description: The name of the sauce.
    type: string
    exposed: true
    stored: true

  - name: type
    description: Indicates the spiceness is vegan.
    type: string
    exposed: true
    stored: true
    allowed_choices:
    - Red
    - White
    - Custom
    default_value: Red

  - name: vegan
    description: Indicates the sauce is vegan.
    type: boolean
    exposed: true
    stored: true
```

### Topping

```yaml
attributes:
  - name: description
    description: The description of the topping.
    type: string
    exposed: true
    stored: true

  - name: name
    description: The name of the topping.
    type: string
    exposed: true
    stored: true

  - name: spiciness
    description: Indicates the spiceness is vegan.
    type: string
    exposed: true
    stored: true
    allowed_choices:
    - None
    - Mild
    - Hot
    default_value: None

  - name: vegan
    description: Indicates the topping is vegan.
    type: boolean
    exposed: true
    stored: true
```

## Running the backend

It is recommended to run the backend using 

```
make compose
```

Once the backend is running, you can run a script to initialize some data:

```
challenge/init-data.sh
```

Now, you can list all the toppings using:
```
curl http://127.0.0.1:3443/toppings
```

or filter using:
```
curl http://127.0.0.1:3443/toppings\?q\=name\=\="cheese"
```

# Challenge

This backend is used for two types of challenges:

* Backend
* UI