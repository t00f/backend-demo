#!/bin/bash

function add() {
  curl -k -X POST "https://127.0.0.1:3443/$1" \
    -H 'Content-Type: application/json' "${@:2}"
}

echo "Adding sauces..."
add sauces -d '{
    "name": "Tomato",
    "description": "The traditional tomato sauce with provence herbs",
    "type": "Red"
    }'

add sauces -d '{
    "name": "Tomato",
    "description": "The traditional tomato sauce with provence herbs",
    "type": "Red"
    }'

add sauces -d '{
    "name": "White",
    "description": "A delicious creamy sauce",
    "type": "White"
    }'

echo "Adding doughs..."
add doughs -d '{
    "name": "Italian Traditional",
    "description": "A think crust cooked at high temperature",
    "type": "Napolitan"
    }'

add doughs -d '{
    "name": "American Traditional",
    "description": "The most famous american pizza",
    "type": "NewYork"
    }'
add doughs -d '{
    "name": "Cheesy",
    "description": "The ultimate dough that has cheese in it",
    "type": "Cheesecrust"
    }'

echo "Adding toppings..."
add toppings -d '{
    "name": "pepperoni",
    "description": "Add spicy sausage to give life to your pizza",
    "spiciness": "Mild",
    "vegan": false
    }'
add toppings -d '{
    "name": "mushroom",
    "description": "Add some white mushroom to your pizza",
    "spiciness": "None",
    "vegan": true
    }'
add toppings -d '{
    "name": "Swiss cheese",
    "description": "Add some swiss cheese",
    "spiciness": "None",
    "vegan": false
    }'

add toppings -d '{
    "name": "Mozarella cheese",
    "description": "Add some mozarella cheese",
    "spiciness": "None",
    "vegan": false
    }'

add toppings -d '{
    "name": "pepper",
    "description": "Add some spiciness to your pizza",
    "spiciness": "Hot",
    "vegan": true
    }'



### To go further, here is an example on how to use filters
### And create a pizza full of cheese!

echo "Adding one pizza..."

SAUCE=$(curl -k -H 'Content-Type: application/json' "https://127.0.0.1:3443/sauces?q=name==Tomato" | jq '.[0]')
DOUGH=$(curl -k -H 'Content-Type: application/json' "https://127.0.0.1:3443/doughs?q=name==Cheesy" | jq '.[0]')
TOPPINGS=$(curl -k -H 'Content-Type: application/json' "https://127.0.0.1:3443/toppings?q=name%20matches%20cheese")

function _payload() {
    cat <<EOF
{
    "name": "Cheese", 
    "description": "Full cheese pizza", 
    "dough": $DOUGH, 
    "sauce": $SAUCE, 
    "toppings": $TOPPINGS
}
EOF
}

add pizzas -d "$(_payload)"
