module gitlab.com/t00f/backend-demo

go 1.16

require (
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/mitchellh/copystructure v1.2.0
	go.aporeto.io/addedeffect v1.77.1-0.20211019221207-6e4f1f269ab1
	go.aporeto.io/bahamut v1.112.1-0.20211108183110-26de36f87bcb
	go.aporeto.io/elemental v1.100.1-0.20211007231947-44c1aabe7914
	go.aporeto.io/manipulate v1.121.1-0.20211019221115-0ade7e9c8369
	go.aporeto.io/tg v1.34.1-0.20210528201128-159c302ba155
	go.uber.org/zap v1.19.0
)
