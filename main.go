package main

import (
	"fmt"
	"os"

	"gitlab.com/t00f/backend-demo/backend/gateway"
	"gitlab.com/t00f/backend-demo/backend/order"
	"gitlab.com/t00f/backend-demo/backend/pizza"
)

const usage = "Pass the service name"

func main() {

	if len(os.Args) < 2 {
		fmt.Fprint(os.Stderr, usage)
		os.Exit(1)
	}
	start()
}

func start() {

	switch os.Args[1] {
	case "pizza":
		pizza.Start()
	case "order":
		order.Start()
	case "gateway":
		gateway.Start()
	default:
		fmt.Fprint(os.Stderr, "unknown service name")
		os.Exit(1)
	}
}
