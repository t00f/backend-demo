package models

import (
	"net/http"
	"strings"

	"go.aporeto.io/elemental"
)

// makeValidationError returns an elemental error for the validation.
func makeValidationError(attribute string, message string) elemental.Error {
	err := elemental.NewError(
		"Validation Error",
		message,
		"models",
		http.StatusUnprocessableEntity,
	)

	if attribute != "" {
		err.Data = map[string]interface{}{"attribute": attribute}
	}

	return err
}

// ValidatePizzaName validates the pizza name.
func ValidatePizzaName(attribute string, value string) error {
	if strings.Contains(value, "!") {
		return makeValidationError(attribute, "pizza name cannot contain the character '!'")
	}
	return nil
}

// ValidateOrder validates the model Order.
func ValidateOrder(order *Order) error {

	if len(order.Items) == 0 {
		return makeValidationError("items", "no items found in this order")
	}

	return nil
}
