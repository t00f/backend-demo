package models

import (
	"fmt"

	"github.com/globalsign/mgo/bson"
	"github.com/mitchellh/copystructure"
	"go.aporeto.io/elemental"
)

// DoughStyleValue represents the possible values for attribute "style".
type DoughStyleValue string

const (
	// DoughStyleCheesecrust represents the value Cheesecrust.
	DoughStyleCheesecrust DoughStyleValue = "Cheesecrust"

	// DoughStyleNapolitan represents the value Napolitan.
	DoughStyleNapolitan DoughStyleValue = "Napolitan"

	// DoughStyleNewYork represents the value NewYork.
	DoughStyleNewYork DoughStyleValue = "NewYork"

	// DoughStyleSicilian represents the value Sicilian.
	DoughStyleSicilian DoughStyleValue = "Sicilian"
)

// DoughIdentity represents the Identity of the object.
var DoughIdentity = elemental.Identity{
	Name:     "dough",
	Category: "doughs",
	Package:  "default",
	Private:  false,
}

// DoughsList represents a list of Doughs
type DoughsList []*Dough

// Identity returns the identity of the objects in the list.
func (o DoughsList) Identity() elemental.Identity {

	return DoughIdentity
}

// Copy returns a pointer to a copy the DoughsList.
func (o DoughsList) Copy() elemental.Identifiables {

	copy := append(DoughsList{}, o...)
	return &copy
}

// Append appends the objects to the a new copy of the DoughsList.
func (o DoughsList) Append(objects ...elemental.Identifiable) elemental.Identifiables {

	out := append(DoughsList{}, o...)
	for _, obj := range objects {
		out = append(out, obj.(*Dough))
	}

	return out
}

// List converts the object to an elemental.IdentifiablesList.
func (o DoughsList) List() elemental.IdentifiablesList {

	out := make(elemental.IdentifiablesList, len(o))
	for i := 0; i < len(o); i++ {
		out[i] = o[i]
	}

	return out
}

// DefaultOrder returns the default ordering fields of the content.
func (o DoughsList) DefaultOrder() []string {

	return []string{}
}

// ToSparse returns the DoughsList converted to SparseDoughsList.
// Objects in the list will only contain the given fields. No field means entire field set.
func (o DoughsList) ToSparse(fields ...string) elemental.Identifiables {

	out := make(SparseDoughsList, len(o))
	for i := 0; i < len(o); i++ {
		out[i] = o[i].ToSparse(fields...).(*SparseDough)
	}

	return out
}

// Version returns the version of the content.
func (o DoughsList) Version() int {

	return 1
}

// Dough represents the model of a dough
type Dough struct {
	// ID is the identifier of the object.
	ID string `json:"ID" msgpack:"ID" bson:"-" mapstructure:"ID,omitempty"`

	// The description of the dough.
	Description string `json:"description" msgpack:"description" bson:"description" mapstructure:"description,omitempty"`

	// The name of the dough.
	Name string `json:"name" msgpack:"name" bson:"name" mapstructure:"name,omitempty"`

	// The style of the dough.
	Style DoughStyleValue `json:"style" msgpack:"style" bson:"style" mapstructure:"style,omitempty"`

	ModelVersion int `json:"-" msgpack:"-" bson:"_modelversion"`
}

// NewDough returns a new *Dough
func NewDough() *Dough {

	return &Dough{
		ModelVersion: 1,
		Style:        DoughStyleNewYork,
	}
}

// Identity returns the Identity of the object.
func (o *Dough) Identity() elemental.Identity {

	return DoughIdentity
}

// Identifier returns the value of the object's unique identifier.
func (o *Dough) Identifier() string {

	return o.ID
}

// SetIdentifier sets the value of the object's unique identifier.
func (o *Dough) SetIdentifier(id string) {

	o.ID = id
}

// GetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *Dough) GetBSON() (interface{}, error) {

	if o == nil {
		return nil, nil
	}

	s := &mongoAttributesDough{}

	if o.ID != "" {
		s.ID = bson.ObjectIdHex(o.ID)
	}
	s.Description = o.Description
	s.Name = o.Name
	s.Style = o.Style

	return s, nil
}

// SetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *Dough) SetBSON(raw bson.Raw) error {

	if o == nil {
		return nil
	}

	s := &mongoAttributesDough{}
	if err := raw.Unmarshal(s); err != nil {
		return err
	}

	o.ID = s.ID.Hex()
	o.Description = s.Description
	o.Name = s.Name
	o.Style = s.Style

	return nil
}

// Version returns the hardcoded version of the model.
func (o *Dough) Version() int {

	return 1
}

// BleveType implements the bleve.Classifier Interface.
func (o *Dough) BleveType() string {

	return "dough"
}

// DefaultOrder returns the list of default ordering fields.
func (o *Dough) DefaultOrder() []string {

	return []string{}
}

// Doc returns the documentation for the object
func (o *Dough) Doc() string {

	return `This is a dough.`
}

func (o *Dough) String() string {

	return fmt.Sprintf("<%s:%s>", o.Identity().Name, o.Identifier())
}

// ToSparse returns the sparse version of the model.
// The returned object will only contain the given fields. No field means entire field set.
func (o *Dough) ToSparse(fields ...string) elemental.SparseIdentifiable {

	if len(fields) == 0 {
		// nolint: goimports
		return &SparseDough{
			ID:          &o.ID,
			Description: &o.Description,
			Name:        &o.Name,
			Style:       &o.Style,
		}
	}

	sp := &SparseDough{}
	for _, f := range fields {
		switch f {
		case "ID":
			sp.ID = &(o.ID)
		case "description":
			sp.Description = &(o.Description)
		case "name":
			sp.Name = &(o.Name)
		case "style":
			sp.Style = &(o.Style)
		}
	}

	return sp
}

// Patch apply the non nil value of a *SparseDough to the object.
func (o *Dough) Patch(sparse elemental.SparseIdentifiable) {
	if !sparse.Identity().IsEqual(o.Identity()) {
		panic("cannot patch from a parse with different identity")
	}

	so := sparse.(*SparseDough)
	if so.ID != nil {
		o.ID = *so.ID
	}
	if so.Description != nil {
		o.Description = *so.Description
	}
	if so.Name != nil {
		o.Name = *so.Name
	}
	if so.Style != nil {
		o.Style = *so.Style
	}
}

// DeepCopy returns a deep copy if the Dough.
func (o *Dough) DeepCopy() *Dough {

	if o == nil {
		return nil
	}

	out := &Dough{}
	o.DeepCopyInto(out)

	return out
}

// DeepCopyInto copies the receiver into the given *Dough.
func (o *Dough) DeepCopyInto(out *Dough) {

	target, err := copystructure.Copy(o)
	if err != nil {
		panic(fmt.Sprintf("Unable to deepcopy Dough: %s", err))
	}

	*out = *target.(*Dough)
}

// Validate valides the current information stored into the structure.
func (o *Dough) Validate() error {

	errors := elemental.Errors{}
	requiredErrors := elemental.Errors{}

	if err := elemental.ValidateStringInList("style", string(o.Style), []string{"NewYork", "Napolitan", "Sicilian", "Cheesecrust"}, false); err != nil {
		errors = errors.Append(err)
	}

	if len(requiredErrors) > 0 {
		return requiredErrors
	}

	if len(errors) > 0 {
		return errors
	}

	return nil
}

// SpecificationForAttribute returns the AttributeSpecification for the given attribute name key.
func (*Dough) SpecificationForAttribute(name string) elemental.AttributeSpecification {

	if v, ok := DoughAttributesMap[name]; ok {
		return v
	}

	// We could not find it, so let's check on the lower case indexed spec map
	return DoughLowerCaseAttributesMap[name]
}

// AttributeSpecifications returns the full attribute specifications map.
func (*Dough) AttributeSpecifications() map[string]elemental.AttributeSpecification {

	return DoughAttributesMap
}

// ValueForAttribute returns the value for the given attribute.
// This is a very advanced function that you should not need but in some
// very specific use cases.
func (o *Dough) ValueForAttribute(name string) interface{} {

	switch name {
	case "ID":
		return o.ID
	case "description":
		return o.Description
	case "name":
		return o.Name
	case "style":
		return o.Style
	}

	return nil
}

// DoughAttributesMap represents the map of attribute for Dough.
var DoughAttributesMap = map[string]elemental.AttributeSpecification{
	"ID": {
		AllowedChoices: []string{},
		Autogenerated:  true,
		BSONFieldName:  "_id",
		ConvertedName:  "ID",
		Description:    `ID is the identifier of the object.`,
		Exposed:        true,
		Filterable:     true,
		Identifier:     true,
		Name:           "ID",
		Orderable:      true,
		ReadOnly:       true,
		Stored:         true,
		Type:           "string",
	},
	"Description": {
		AllowedChoices: []string{},
		BSONFieldName:  "description",
		ConvertedName:  "Description",
		Description:    `The description of the dough.`,
		Exposed:        true,
		Name:           "description",
		Stored:         true,
		Type:           "string",
	},
	"Name": {
		AllowedChoices: []string{},
		BSONFieldName:  "name",
		ConvertedName:  "Name",
		Description:    `The name of the dough.`,
		Exposed:        true,
		Name:           "name",
		Stored:         true,
		Type:           "string",
	},
	"Style": {
		AllowedChoices: []string{"NewYork", "Napolitan", "Sicilian", "Cheesecrust"},
		BSONFieldName:  "style",
		ConvertedName:  "Style",
		DefaultValue:   DoughStyleNewYork,
		Description:    `The style of the dough.`,
		Exposed:        true,
		Name:           "style",
		Stored:         true,
		Type:           "enum",
	},
}

// DoughLowerCaseAttributesMap represents the map of attribute for Dough.
var DoughLowerCaseAttributesMap = map[string]elemental.AttributeSpecification{
	"id": {
		AllowedChoices: []string{},
		Autogenerated:  true,
		BSONFieldName:  "_id",
		ConvertedName:  "ID",
		Description:    `ID is the identifier of the object.`,
		Exposed:        true,
		Filterable:     true,
		Identifier:     true,
		Name:           "ID",
		Orderable:      true,
		ReadOnly:       true,
		Stored:         true,
		Type:           "string",
	},
	"description": {
		AllowedChoices: []string{},
		BSONFieldName:  "description",
		ConvertedName:  "Description",
		Description:    `The description of the dough.`,
		Exposed:        true,
		Name:           "description",
		Stored:         true,
		Type:           "string",
	},
	"name": {
		AllowedChoices: []string{},
		BSONFieldName:  "name",
		ConvertedName:  "Name",
		Description:    `The name of the dough.`,
		Exposed:        true,
		Name:           "name",
		Stored:         true,
		Type:           "string",
	},
	"style": {
		AllowedChoices: []string{"NewYork", "Napolitan", "Sicilian", "Cheesecrust"},
		BSONFieldName:  "style",
		ConvertedName:  "Style",
		DefaultValue:   DoughStyleNewYork,
		Description:    `The style of the dough.`,
		Exposed:        true,
		Name:           "style",
		Stored:         true,
		Type:           "enum",
	},
}

// SparseDoughsList represents a list of SparseDoughs
type SparseDoughsList []*SparseDough

// Identity returns the identity of the objects in the list.
func (o SparseDoughsList) Identity() elemental.Identity {

	return DoughIdentity
}

// Copy returns a pointer to a copy the SparseDoughsList.
func (o SparseDoughsList) Copy() elemental.Identifiables {

	copy := append(SparseDoughsList{}, o...)
	return &copy
}

// Append appends the objects to the a new copy of the SparseDoughsList.
func (o SparseDoughsList) Append(objects ...elemental.Identifiable) elemental.Identifiables {

	out := append(SparseDoughsList{}, o...)
	for _, obj := range objects {
		out = append(out, obj.(*SparseDough))
	}

	return out
}

// List converts the object to an elemental.IdentifiablesList.
func (o SparseDoughsList) List() elemental.IdentifiablesList {

	out := make(elemental.IdentifiablesList, len(o))
	for i := 0; i < len(o); i++ {
		out[i] = o[i]
	}

	return out
}

// DefaultOrder returns the default ordering fields of the content.
func (o SparseDoughsList) DefaultOrder() []string {

	return []string{}
}

// ToPlain returns the SparseDoughsList converted to DoughsList.
func (o SparseDoughsList) ToPlain() elemental.IdentifiablesList {

	out := make(elemental.IdentifiablesList, len(o))
	for i := 0; i < len(o); i++ {
		out[i] = o[i].ToPlain()
	}

	return out
}

// Version returns the version of the content.
func (o SparseDoughsList) Version() int {

	return 1
}

// SparseDough represents the sparse version of a dough.
type SparseDough struct {
	// ID is the identifier of the object.
	ID *string `json:"ID,omitempty" msgpack:"ID,omitempty" bson:"-" mapstructure:"ID,omitempty"`

	// The description of the dough.
	Description *string `json:"description,omitempty" msgpack:"description,omitempty" bson:"description,omitempty" mapstructure:"description,omitempty"`

	// The name of the dough.
	Name *string `json:"name,omitempty" msgpack:"name,omitempty" bson:"name,omitempty" mapstructure:"name,omitempty"`

	// The style of the dough.
	Style *DoughStyleValue `json:"style,omitempty" msgpack:"style,omitempty" bson:"style,omitempty" mapstructure:"style,omitempty"`

	ModelVersion int `json:"-" msgpack:"-" bson:"_modelversion"`
}

// NewSparseDough returns a new  SparseDough.
func NewSparseDough() *SparseDough {
	return &SparseDough{}
}

// Identity returns the Identity of the sparse object.
func (o *SparseDough) Identity() elemental.Identity {

	return DoughIdentity
}

// Identifier returns the value of the sparse object's unique identifier.
func (o *SparseDough) Identifier() string {

	if o.ID == nil {
		return ""
	}
	return *o.ID
}

// SetIdentifier sets the value of the sparse object's unique identifier.
func (o *SparseDough) SetIdentifier(id string) {

	if id != "" {
		o.ID = &id
	} else {
		o.ID = nil
	}
}

// GetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *SparseDough) GetBSON() (interface{}, error) {

	if o == nil {
		return nil, nil
	}

	s := &mongoAttributesSparseDough{}

	if o.ID != nil {
		s.ID = bson.ObjectIdHex(*o.ID)
	}
	if o.Description != nil {
		s.Description = o.Description
	}
	if o.Name != nil {
		s.Name = o.Name
	}
	if o.Style != nil {
		s.Style = o.Style
	}

	return s, nil
}

// SetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *SparseDough) SetBSON(raw bson.Raw) error {

	if o == nil {
		return nil
	}

	s := &mongoAttributesSparseDough{}
	if err := raw.Unmarshal(s); err != nil {
		return err
	}

	id := s.ID.Hex()
	o.ID = &id
	if s.Description != nil {
		o.Description = s.Description
	}
	if s.Name != nil {
		o.Name = s.Name
	}
	if s.Style != nil {
		o.Style = s.Style
	}

	return nil
}

// Version returns the hardcoded version of the model.
func (o *SparseDough) Version() int {

	return 1
}

// ToPlain returns the plain version of the sparse model.
func (o *SparseDough) ToPlain() elemental.PlainIdentifiable {

	out := NewDough()
	if o.ID != nil {
		out.ID = *o.ID
	}
	if o.Description != nil {
		out.Description = *o.Description
	}
	if o.Name != nil {
		out.Name = *o.Name
	}
	if o.Style != nil {
		out.Style = *o.Style
	}

	return out
}

// DeepCopy returns a deep copy if the SparseDough.
func (o *SparseDough) DeepCopy() *SparseDough {

	if o == nil {
		return nil
	}

	out := &SparseDough{}
	o.DeepCopyInto(out)

	return out
}

// DeepCopyInto copies the receiver into the given *SparseDough.
func (o *SparseDough) DeepCopyInto(out *SparseDough) {

	target, err := copystructure.Copy(o)
	if err != nil {
		panic(fmt.Sprintf("Unable to deepcopy SparseDough: %s", err))
	}

	*out = *target.(*SparseDough)
}

type mongoAttributesDough struct {
	ID          bson.ObjectId   `bson:"_id,omitempty"`
	Description string          `bson:"description"`
	Name        string          `bson:"name"`
	Style       DoughStyleValue `bson:"style"`
}
type mongoAttributesSparseDough struct {
	ID          bson.ObjectId    `bson:"_id,omitempty"`
	Description *string          `bson:"description,omitempty"`
	Name        *string          `bson:"name,omitempty"`
	Style       *DoughStyleValue `bson:"style,omitempty"`
}
