package models

import "go.aporeto.io/elemental"

var (
	identityNamesMap = map[string]elemental.Identity{
		"dough": DoughIdentity,
		"order": OrderIdentity,

		"pizza":   PizzaIdentity,
		"root":    RootIdentity,
		"sauce":   SauceIdentity,
		"topping": ToppingIdentity,
	}

	identitycategoriesMap = map[string]elemental.Identity{
		"doughs": DoughIdentity,
		"orders": OrderIdentity,

		"pizzas":   PizzaIdentity,
		"root":     RootIdentity,
		"sauces":   SauceIdentity,
		"toppings": ToppingIdentity,
	}

	aliasesMap = map[string]elemental.Identity{}

	indexesMap = map[string][][]string{
		"dough":   nil,
		"order":   nil,
		"pizza":   nil,
		"root":    nil,
		"sauce":   nil,
		"topping": nil,
	}
)

// ModelVersion returns the current version of the model.
func ModelVersion() float64 { return 1 }

type modelManager struct{}

func (f modelManager) IdentityFromName(name string) elemental.Identity {

	return identityNamesMap[name]
}

func (f modelManager) IdentityFromCategory(category string) elemental.Identity {

	return identitycategoriesMap[category]
}

func (f modelManager) IdentityFromAlias(alias string) elemental.Identity {

	return aliasesMap[alias]
}

func (f modelManager) IdentityFromAny(any string) (i elemental.Identity) {

	if i = f.IdentityFromName(any); !i.IsEmpty() {
		return i
	}

	if i = f.IdentityFromCategory(any); !i.IsEmpty() {
		return i
	}

	return f.IdentityFromAlias(any)
}

func (f modelManager) Identifiable(identity elemental.Identity) elemental.Identifiable {

	switch identity {

	case DoughIdentity:
		return NewDough()
	case OrderIdentity:
		return NewOrder()
	case PizzaIdentity:
		return NewPizza()
	case RootIdentity:
		return NewRoot()
	case SauceIdentity:
		return NewSauce()
	case ToppingIdentity:
		return NewTopping()
	default:
		return nil
	}
}

func (f modelManager) SparseIdentifiable(identity elemental.Identity) elemental.SparseIdentifiable {

	switch identity {

	case DoughIdentity:
		return NewSparseDough()
	case OrderIdentity:
		return NewSparseOrder()
	case PizzaIdentity:
		return NewSparsePizza()
	case SauceIdentity:
		return NewSparseSauce()
	case ToppingIdentity:
		return NewSparseTopping()
	default:
		return nil
	}
}

func (f modelManager) Indexes(identity elemental.Identity) [][]string {

	return indexesMap[identity.Name]
}

func (f modelManager) IdentifiableFromString(any string) elemental.Identifiable {

	return f.Identifiable(f.IdentityFromAny(any))
}

func (f modelManager) Identifiables(identity elemental.Identity) elemental.Identifiables {

	switch identity {

	case DoughIdentity:
		return &DoughsList{}
	case OrderIdentity:
		return &OrdersList{}
	case PizzaIdentity:
		return &PizzasList{}
	case SauceIdentity:
		return &SaucesList{}
	case ToppingIdentity:
		return &ToppingsList{}
	default:
		return nil
	}
}

func (f modelManager) SparseIdentifiables(identity elemental.Identity) elemental.SparseIdentifiables {

	switch identity {

	case DoughIdentity:
		return &SparseDoughsList{}
	case OrderIdentity:
		return &SparseOrdersList{}
	case PizzaIdentity:
		return &SparsePizzasList{}
	case SauceIdentity:
		return &SparseSaucesList{}
	case ToppingIdentity:
		return &SparseToppingsList{}
	default:
		return nil
	}
}

func (f modelManager) IdentifiablesFromString(any string) elemental.Identifiables {

	return f.Identifiables(f.IdentityFromAny(any))
}

func (f modelManager) Relationships() elemental.RelationshipsRegistry {

	return relationshipsRegistry
}

func (f modelManager) AllIdentities() []elemental.Identity {
	return AllIdentities()
}

var manager = modelManager{}

// Manager returns the model elemental.ModelManager.
func Manager() elemental.ModelManager { return manager }

// AllIdentities returns all existing identities.
func AllIdentities() []elemental.Identity {

	return []elemental.Identity{
		DoughIdentity,
		OrderIdentity,
		PizzaIdentity,
		RootIdentity,
		SauceIdentity,
		ToppingIdentity,
	}
}

// AliasesForIdentity returns all the aliases for the given identity.
func AliasesForIdentity(identity elemental.Identity) []string {

	switch identity {
	case DoughIdentity:
		return []string{}
	case OrderIdentity:
		return []string{}
	case PizzaIdentity:
		return []string{}
	case RootIdentity:
		return []string{}
	case SauceIdentity:
		return []string{}
	case ToppingIdentity:
		return []string{}
	}

	return nil
}
