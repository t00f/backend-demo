package models

import (
	"fmt"

	"github.com/globalsign/mgo/bson"
	"github.com/mitchellh/copystructure"
	"go.aporeto.io/elemental"
)

// OrderIdentity represents the Identity of the object.
var OrderIdentity = elemental.Identity{
	Name:     "order",
	Category: "orders",
	Package:  "core",
	Private:  false,
}

// OrdersList represents a list of Orders
type OrdersList []*Order

// Identity returns the identity of the objects in the list.
func (o OrdersList) Identity() elemental.Identity {

	return OrderIdentity
}

// Copy returns a pointer to a copy the OrdersList.
func (o OrdersList) Copy() elemental.Identifiables {

	copy := append(OrdersList{}, o...)
	return &copy
}

// Append appends the objects to the a new copy of the OrdersList.
func (o OrdersList) Append(objects ...elemental.Identifiable) elemental.Identifiables {

	out := append(OrdersList{}, o...)
	for _, obj := range objects {
		out = append(out, obj.(*Order))
	}

	return out
}

// List converts the object to an elemental.IdentifiablesList.
func (o OrdersList) List() elemental.IdentifiablesList {

	out := make(elemental.IdentifiablesList, len(o))
	for i := 0; i < len(o); i++ {
		out[i] = o[i]
	}

	return out
}

// DefaultOrder returns the default ordering fields of the content.
func (o OrdersList) DefaultOrder() []string {

	return []string{}
}

// ToSparse returns the OrdersList converted to SparseOrdersList.
// Objects in the list will only contain the given fields. No field means entire field set.
func (o OrdersList) ToSparse(fields ...string) elemental.Identifiables {

	out := make(SparseOrdersList, len(o))
	for i := 0; i < len(o); i++ {
		out[i] = o[i].ToSparse(fields...).(*SparseOrder)
	}

	return out
}

// Version returns the version of the content.
func (o OrdersList) Version() int {

	return 1
}

// Order represents the model of a order
type Order struct {
	// ID is the identifier of the object.
	ID string `json:"ID" msgpack:"ID" bson:"-" mapstructure:"ID,omitempty"`

	// List of items of the order.
	Items []*OrderItem `json:"items" msgpack:"items" bson:"items" mapstructure:"items,omitempty"`

	ModelVersion int `json:"-" msgpack:"-" bson:"_modelversion"`
}

// NewOrder returns a new *Order
func NewOrder() *Order {

	return &Order{
		ModelVersion: 1,
		Items:        []*OrderItem{},
	}
}

// Identity returns the Identity of the object.
func (o *Order) Identity() elemental.Identity {

	return OrderIdentity
}

// Identifier returns the value of the object's unique identifier.
func (o *Order) Identifier() string {

	return o.ID
}

// SetIdentifier sets the value of the object's unique identifier.
func (o *Order) SetIdentifier(id string) {

	o.ID = id
}

// GetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *Order) GetBSON() (interface{}, error) {

	if o == nil {
		return nil, nil
	}

	s := &mongoAttributesOrder{}

	if o.ID != "" {
		s.ID = bson.ObjectIdHex(o.ID)
	}
	s.Items = o.Items

	return s, nil
}

// SetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *Order) SetBSON(raw bson.Raw) error {

	if o == nil {
		return nil
	}

	s := &mongoAttributesOrder{}
	if err := raw.Unmarshal(s); err != nil {
		return err
	}

	o.ID = s.ID.Hex()
	o.Items = s.Items

	return nil
}

// Version returns the hardcoded version of the model.
func (o *Order) Version() int {

	return 1
}

// BleveType implements the bleve.Classifier Interface.
func (o *Order) BleveType() string {

	return "order"
}

// DefaultOrder returns the list of default ordering fields.
func (o *Order) DefaultOrder() []string {

	return []string{}
}

// Doc returns the documentation for the object
func (o *Order) Doc() string {

	return `This is an order.`
}

func (o *Order) String() string {

	return fmt.Sprintf("<%s:%s>", o.Identity().Name, o.Identifier())
}

// ToSparse returns the sparse version of the model.
// The returned object will only contain the given fields. No field means entire field set.
func (o *Order) ToSparse(fields ...string) elemental.SparseIdentifiable {

	if len(fields) == 0 {
		// nolint: goimports
		return &SparseOrder{
			ID:    &o.ID,
			Items: &o.Items,
		}
	}

	sp := &SparseOrder{}
	for _, f := range fields {
		switch f {
		case "ID":
			sp.ID = &(o.ID)
		case "items":
			sp.Items = &(o.Items)
		}
	}

	return sp
}

// Patch apply the non nil value of a *SparseOrder to the object.
func (o *Order) Patch(sparse elemental.SparseIdentifiable) {
	if !sparse.Identity().IsEqual(o.Identity()) {
		panic("cannot patch from a parse with different identity")
	}

	so := sparse.(*SparseOrder)
	if so.ID != nil {
		o.ID = *so.ID
	}
	if so.Items != nil {
		o.Items = *so.Items
	}
}

// DeepCopy returns a deep copy if the Order.
func (o *Order) DeepCopy() *Order {

	if o == nil {
		return nil
	}

	out := &Order{}
	o.DeepCopyInto(out)

	return out
}

// DeepCopyInto copies the receiver into the given *Order.
func (o *Order) DeepCopyInto(out *Order) {

	target, err := copystructure.Copy(o)
	if err != nil {
		panic(fmt.Sprintf("Unable to deepcopy Order: %s", err))
	}

	*out = *target.(*Order)
}

// Validate valides the current information stored into the structure.
func (o *Order) Validate() error {

	errors := elemental.Errors{}
	requiredErrors := elemental.Errors{}

	for _, sub := range o.Items {
		if sub == nil {
			continue
		}
		elemental.ResetDefaultForZeroValues(sub)
		if err := sub.Validate(); err != nil {
			errors = errors.Append(err)
		}
	}

	// Custom object validation.
	if err := ValidateOrder(o); err != nil {
		errors = errors.Append(err)
	}

	if len(requiredErrors) > 0 {
		return requiredErrors
	}

	if len(errors) > 0 {
		return errors
	}

	return nil
}

// SpecificationForAttribute returns the AttributeSpecification for the given attribute name key.
func (*Order) SpecificationForAttribute(name string) elemental.AttributeSpecification {

	if v, ok := OrderAttributesMap[name]; ok {
		return v
	}

	// We could not find it, so let's check on the lower case indexed spec map
	return OrderLowerCaseAttributesMap[name]
}

// AttributeSpecifications returns the full attribute specifications map.
func (*Order) AttributeSpecifications() map[string]elemental.AttributeSpecification {

	return OrderAttributesMap
}

// ValueForAttribute returns the value for the given attribute.
// This is a very advanced function that you should not need but in some
// very specific use cases.
func (o *Order) ValueForAttribute(name string) interface{} {

	switch name {
	case "ID":
		return o.ID
	case "items":
		return o.Items
	}

	return nil
}

// OrderAttributesMap represents the map of attribute for Order.
var OrderAttributesMap = map[string]elemental.AttributeSpecification{
	"ID": {
		AllowedChoices: []string{},
		Autogenerated:  true,
		BSONFieldName:  "_id",
		ConvertedName:  "ID",
		Description:    `ID is the identifier of the object.`,
		Exposed:        true,
		Filterable:     true,
		Identifier:     true,
		Name:           "ID",
		Orderable:      true,
		ReadOnly:       true,
		Stored:         true,
		Type:           "string",
	},
	"Items": {
		AllowedChoices: []string{},
		BSONFieldName:  "items",
		ConvertedName:  "Items",
		Description:    `List of items of the order.`,
		Exposed:        true,
		Name:           "items",
		Stored:         true,
		SubType:        "orderitem",
		Type:           "refList",
	},
}

// OrderLowerCaseAttributesMap represents the map of attribute for Order.
var OrderLowerCaseAttributesMap = map[string]elemental.AttributeSpecification{
	"id": {
		AllowedChoices: []string{},
		Autogenerated:  true,
		BSONFieldName:  "_id",
		ConvertedName:  "ID",
		Description:    `ID is the identifier of the object.`,
		Exposed:        true,
		Filterable:     true,
		Identifier:     true,
		Name:           "ID",
		Orderable:      true,
		ReadOnly:       true,
		Stored:         true,
		Type:           "string",
	},
	"items": {
		AllowedChoices: []string{},
		BSONFieldName:  "items",
		ConvertedName:  "Items",
		Description:    `List of items of the order.`,
		Exposed:        true,
		Name:           "items",
		Stored:         true,
		SubType:        "orderitem",
		Type:           "refList",
	},
}

// SparseOrdersList represents a list of SparseOrders
type SparseOrdersList []*SparseOrder

// Identity returns the identity of the objects in the list.
func (o SparseOrdersList) Identity() elemental.Identity {

	return OrderIdentity
}

// Copy returns a pointer to a copy the SparseOrdersList.
func (o SparseOrdersList) Copy() elemental.Identifiables {

	copy := append(SparseOrdersList{}, o...)
	return &copy
}

// Append appends the objects to the a new copy of the SparseOrdersList.
func (o SparseOrdersList) Append(objects ...elemental.Identifiable) elemental.Identifiables {

	out := append(SparseOrdersList{}, o...)
	for _, obj := range objects {
		out = append(out, obj.(*SparseOrder))
	}

	return out
}

// List converts the object to an elemental.IdentifiablesList.
func (o SparseOrdersList) List() elemental.IdentifiablesList {

	out := make(elemental.IdentifiablesList, len(o))
	for i := 0; i < len(o); i++ {
		out[i] = o[i]
	}

	return out
}

// DefaultOrder returns the default ordering fields of the content.
func (o SparseOrdersList) DefaultOrder() []string {

	return []string{}
}

// ToPlain returns the SparseOrdersList converted to OrdersList.
func (o SparseOrdersList) ToPlain() elemental.IdentifiablesList {

	out := make(elemental.IdentifiablesList, len(o))
	for i := 0; i < len(o); i++ {
		out[i] = o[i].ToPlain()
	}

	return out
}

// Version returns the version of the content.
func (o SparseOrdersList) Version() int {

	return 1
}

// SparseOrder represents the sparse version of a order.
type SparseOrder struct {
	// ID is the identifier of the object.
	ID *string `json:"ID,omitempty" msgpack:"ID,omitempty" bson:"-" mapstructure:"ID,omitempty"`

	// List of items of the order.
	Items *[]*OrderItem `json:"items,omitempty" msgpack:"items,omitempty" bson:"items,omitempty" mapstructure:"items,omitempty"`

	ModelVersion int `json:"-" msgpack:"-" bson:"_modelversion"`
}

// NewSparseOrder returns a new  SparseOrder.
func NewSparseOrder() *SparseOrder {
	return &SparseOrder{}
}

// Identity returns the Identity of the sparse object.
func (o *SparseOrder) Identity() elemental.Identity {

	return OrderIdentity
}

// Identifier returns the value of the sparse object's unique identifier.
func (o *SparseOrder) Identifier() string {

	if o.ID == nil {
		return ""
	}
	return *o.ID
}

// SetIdentifier sets the value of the sparse object's unique identifier.
func (o *SparseOrder) SetIdentifier(id string) {

	if id != "" {
		o.ID = &id
	} else {
		o.ID = nil
	}
}

// GetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *SparseOrder) GetBSON() (interface{}, error) {

	if o == nil {
		return nil, nil
	}

	s := &mongoAttributesSparseOrder{}

	if o.ID != nil {
		s.ID = bson.ObjectIdHex(*o.ID)
	}
	if o.Items != nil {
		s.Items = o.Items
	}

	return s, nil
}

// SetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *SparseOrder) SetBSON(raw bson.Raw) error {

	if o == nil {
		return nil
	}

	s := &mongoAttributesSparseOrder{}
	if err := raw.Unmarshal(s); err != nil {
		return err
	}

	id := s.ID.Hex()
	o.ID = &id
	if s.Items != nil {
		o.Items = s.Items
	}

	return nil
}

// Version returns the hardcoded version of the model.
func (o *SparseOrder) Version() int {

	return 1
}

// ToPlain returns the plain version of the sparse model.
func (o *SparseOrder) ToPlain() elemental.PlainIdentifiable {

	out := NewOrder()
	if o.ID != nil {
		out.ID = *o.ID
	}
	if o.Items != nil {
		out.Items = *o.Items
	}

	return out
}

// DeepCopy returns a deep copy if the SparseOrder.
func (o *SparseOrder) DeepCopy() *SparseOrder {

	if o == nil {
		return nil
	}

	out := &SparseOrder{}
	o.DeepCopyInto(out)

	return out
}

// DeepCopyInto copies the receiver into the given *SparseOrder.
func (o *SparseOrder) DeepCopyInto(out *SparseOrder) {

	target, err := copystructure.Copy(o)
	if err != nil {
		panic(fmt.Sprintf("Unable to deepcopy SparseOrder: %s", err))
	}

	*out = *target.(*SparseOrder)
}

type mongoAttributesOrder struct {
	ID    bson.ObjectId `bson:"_id,omitempty"`
	Items []*OrderItem  `bson:"items"`
}
type mongoAttributesSparseOrder struct {
	ID    bson.ObjectId `bson:"_id,omitempty"`
	Items *[]*OrderItem `bson:"items,omitempty"`
}
