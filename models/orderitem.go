package models

import (
	"fmt"

	"github.com/globalsign/mgo/bson"
	"github.com/mitchellh/copystructure"
	"go.aporeto.io/elemental"
)

// OrderItemItemTypeValue represents the possible values for attribute "itemType".
type OrderItemItemTypeValue string

const (
	// OrderItemItemTypePizza represents the value Pizza.
	OrderItemItemTypePizza OrderItemItemTypeValue = "Pizza"
)

// OrderItem represents the model of a orderitem
type OrderItem struct {
	// ID is the identifier of the object.
	ID string `json:"ID" msgpack:"ID" bson:"-" mapstructure:"ID,omitempty"`

	// Type of item. For now, we're only doing pizza.
	ItemType OrderItemItemTypeValue `json:"itemType" msgpack:"itemType" bson:"itemtype" mapstructure:"itemType,omitempty"`

	// Quantity of item to order.
	Quantity int `json:"quantity" msgpack:"quantity" bson:"quantity" mapstructure:"quantity,omitempty"`

	ModelVersion int `json:"-" msgpack:"-" bson:"_modelversion"`
}

// NewOrderItem returns a new *OrderItem
func NewOrderItem() *OrderItem {

	return &OrderItem{
		ModelVersion: 1,
		ItemType:     OrderItemItemTypePizza,
		Quantity:     0,
	}
}

// GetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *OrderItem) GetBSON() (interface{}, error) {

	if o == nil {
		return nil, nil
	}

	s := &mongoAttributesOrderItem{}

	if o.ID != "" {
		s.ID = bson.ObjectIdHex(o.ID)
	}
	s.ItemType = o.ItemType
	s.Quantity = o.Quantity

	return s, nil
}

// SetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *OrderItem) SetBSON(raw bson.Raw) error {

	if o == nil {
		return nil
	}

	s := &mongoAttributesOrderItem{}
	if err := raw.Unmarshal(s); err != nil {
		return err
	}

	o.ID = s.ID.Hex()
	o.ItemType = s.ItemType
	o.Quantity = s.Quantity

	return nil
}

// BleveType implements the bleve.Classifier Interface.
func (o *OrderItem) BleveType() string {

	return "orderitem"
}

// DeepCopy returns a deep copy if the OrderItem.
func (o *OrderItem) DeepCopy() *OrderItem {

	if o == nil {
		return nil
	}

	out := &OrderItem{}
	o.DeepCopyInto(out)

	return out
}

// DeepCopyInto copies the receiver into the given *OrderItem.
func (o *OrderItem) DeepCopyInto(out *OrderItem) {

	target, err := copystructure.Copy(o)
	if err != nil {
		panic(fmt.Sprintf("Unable to deepcopy OrderItem: %s", err))
	}

	*out = *target.(*OrderItem)
}

// Validate valides the current information stored into the structure.
func (o *OrderItem) Validate() error {

	errors := elemental.Errors{}
	requiredErrors := elemental.Errors{}

	if err := elemental.ValidateRequiredString("itemType", string(o.ItemType)); err != nil {
		requiredErrors = requiredErrors.Append(err)
	}

	if err := elemental.ValidateStringInList("itemType", string(o.ItemType), []string{"Pizza"}, false); err != nil {
		errors = errors.Append(err)
	}

	if err := elemental.ValidateRequiredInt("quantity", o.Quantity); err != nil {
		requiredErrors = requiredErrors.Append(err)
	}

	if len(requiredErrors) > 0 {
		return requiredErrors
	}

	if len(errors) > 0 {
		return errors
	}

	return nil
}

type mongoAttributesOrderItem struct {
	ID       bson.ObjectId          `bson:"_id,omitempty"`
	ItemType OrderItemItemTypeValue `bson:"itemtype"`
	Quantity int                    `bson:"quantity"`
}
