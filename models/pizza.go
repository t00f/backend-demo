package models

import (
	"fmt"

	"github.com/globalsign/mgo/bson"
	"github.com/mitchellh/copystructure"
	"go.aporeto.io/elemental"
)

// PizzaIdentity represents the Identity of the object.
var PizzaIdentity = elemental.Identity{
	Name:     "pizza",
	Category: "pizzas",
	Package:  "core",
	Private:  false,
}

// PizzasList represents a list of Pizzas
type PizzasList []*Pizza

// Identity returns the identity of the objects in the list.
func (o PizzasList) Identity() elemental.Identity {

	return PizzaIdentity
}

// Copy returns a pointer to a copy the PizzasList.
func (o PizzasList) Copy() elemental.Identifiables {

	copy := append(PizzasList{}, o...)
	return &copy
}

// Append appends the objects to the a new copy of the PizzasList.
func (o PizzasList) Append(objects ...elemental.Identifiable) elemental.Identifiables {

	out := append(PizzasList{}, o...)
	for _, obj := range objects {
		out = append(out, obj.(*Pizza))
	}

	return out
}

// List converts the object to an elemental.IdentifiablesList.
func (o PizzasList) List() elemental.IdentifiablesList {

	out := make(elemental.IdentifiablesList, len(o))
	for i := 0; i < len(o); i++ {
		out[i] = o[i]
	}

	return out
}

// DefaultOrder returns the default ordering fields of the content.
func (o PizzasList) DefaultOrder() []string {

	return []string{}
}

// ToSparse returns the PizzasList converted to SparsePizzasList.
// Objects in the list will only contain the given fields. No field means entire field set.
func (o PizzasList) ToSparse(fields ...string) elemental.Identifiables {

	out := make(SparsePizzasList, len(o))
	for i := 0; i < len(o); i++ {
		out[i] = o[i].ToSparse(fields...).(*SparsePizza)
	}

	return out
}

// Version returns the version of the content.
func (o PizzasList) Version() int {

	return 1
}

// Pizza represents the model of a pizza
type Pizza struct {
	// ID is the identifier of the object.
	ID string `json:"ID" msgpack:"ID" bson:"-" mapstructure:"ID,omitempty"`

	// The description of the pizza.
	Description string `json:"description" msgpack:"description" bson:"description" mapstructure:"description,omitempty"`

	// The dough of the pizza.
	Dough *Dough `json:"dough" msgpack:"dough" bson:"dough" mapstructure:"dough,omitempty"`

	// The name of the pizza.
	Name string `json:"name" msgpack:"name" bson:"name" mapstructure:"name,omitempty"`

	// The sauce of the pizza.
	Sauce *Sauce `json:"sauce" msgpack:"sauce" bson:"sauce" mapstructure:"sauce,omitempty"`

	// Toppings on the pizza.
	Toppings ToppingsList `json:"toppings" msgpack:"toppings" bson:"toppings" mapstructure:"toppings,omitempty"`

	ModelVersion int `json:"-" msgpack:"-" bson:"_modelversion"`
}

// NewPizza returns a new *Pizza
func NewPizza() *Pizza {

	return &Pizza{
		ModelVersion: 1,
		Toppings:     ToppingsList{},
	}
}

// Identity returns the Identity of the object.
func (o *Pizza) Identity() elemental.Identity {

	return PizzaIdentity
}

// Identifier returns the value of the object's unique identifier.
func (o *Pizza) Identifier() string {

	return o.ID
}

// SetIdentifier sets the value of the object's unique identifier.
func (o *Pizza) SetIdentifier(id string) {

	o.ID = id
}

// GetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *Pizza) GetBSON() (interface{}, error) {

	if o == nil {
		return nil, nil
	}

	s := &mongoAttributesPizza{}

	if o.ID != "" {
		s.ID = bson.ObjectIdHex(o.ID)
	}
	s.Description = o.Description
	s.Dough = o.Dough
	s.Name = o.Name
	s.Sauce = o.Sauce
	s.Toppings = o.Toppings

	return s, nil
}

// SetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *Pizza) SetBSON(raw bson.Raw) error {

	if o == nil {
		return nil
	}

	s := &mongoAttributesPizza{}
	if err := raw.Unmarshal(s); err != nil {
		return err
	}

	o.ID = s.ID.Hex()
	o.Description = s.Description
	o.Dough = s.Dough
	o.Name = s.Name
	o.Sauce = s.Sauce
	o.Toppings = s.Toppings

	return nil
}

// Version returns the hardcoded version of the model.
func (o *Pizza) Version() int {

	return 1
}

// BleveType implements the bleve.Classifier Interface.
func (o *Pizza) BleveType() string {

	return "pizza"
}

// DefaultOrder returns the list of default ordering fields.
func (o *Pizza) DefaultOrder() []string {

	return []string{}
}

// Doc returns the documentation for the object
func (o *Pizza) Doc() string {

	return `This is a Pizza.`
}

func (o *Pizza) String() string {

	return fmt.Sprintf("<%s:%s>", o.Identity().Name, o.Identifier())
}

// ToSparse returns the sparse version of the model.
// The returned object will only contain the given fields. No field means entire field set.
func (o *Pizza) ToSparse(fields ...string) elemental.SparseIdentifiable {

	if len(fields) == 0 {
		// nolint: goimports
		return &SparsePizza{
			ID:          &o.ID,
			Description: &o.Description,
			Dough:       o.Dough,
			Name:        &o.Name,
			Sauce:       o.Sauce,
			Toppings:    &o.Toppings,
		}
	}

	sp := &SparsePizza{}
	for _, f := range fields {
		switch f {
		case "ID":
			sp.ID = &(o.ID)
		case "description":
			sp.Description = &(o.Description)
		case "dough":
			sp.Dough = o.Dough
		case "name":
			sp.Name = &(o.Name)
		case "sauce":
			sp.Sauce = o.Sauce
		case "toppings":
			sp.Toppings = &(o.Toppings)
		}
	}

	return sp
}

// Patch apply the non nil value of a *SparsePizza to the object.
func (o *Pizza) Patch(sparse elemental.SparseIdentifiable) {
	if !sparse.Identity().IsEqual(o.Identity()) {
		panic("cannot patch from a parse with different identity")
	}

	so := sparse.(*SparsePizza)
	if so.ID != nil {
		o.ID = *so.ID
	}
	if so.Description != nil {
		o.Description = *so.Description
	}
	if so.Dough != nil {
		o.Dough = so.Dough
	}
	if so.Name != nil {
		o.Name = *so.Name
	}
	if so.Sauce != nil {
		o.Sauce = so.Sauce
	}
	if so.Toppings != nil {
		o.Toppings = *so.Toppings
	}
}

// DeepCopy returns a deep copy if the Pizza.
func (o *Pizza) DeepCopy() *Pizza {

	if o == nil {
		return nil
	}

	out := &Pizza{}
	o.DeepCopyInto(out)

	return out
}

// DeepCopyInto copies the receiver into the given *Pizza.
func (o *Pizza) DeepCopyInto(out *Pizza) {

	target, err := copystructure.Copy(o)
	if err != nil {
		panic(fmt.Sprintf("Unable to deepcopy Pizza: %s", err))
	}

	*out = *target.(*Pizza)
}

// Validate valides the current information stored into the structure.
func (o *Pizza) Validate() error {

	errors := elemental.Errors{}
	requiredErrors := elemental.Errors{}

	if o.Dough != nil {
		elemental.ResetDefaultForZeroValues(o.Dough)
		if err := o.Dough.Validate(); err != nil {
			errors = errors.Append(err)
		}
	}

	if err := elemental.ValidateRequiredString("name", o.Name); err != nil {
		requiredErrors = requiredErrors.Append(err)
	}

	if err := ValidatePizzaName("name", o.Name); err != nil {
		errors = errors.Append(err)
	}

	if o.Sauce != nil {
		elemental.ResetDefaultForZeroValues(o.Sauce)
		if err := o.Sauce.Validate(); err != nil {
			errors = errors.Append(err)
		}
	}

	for _, sub := range o.Toppings {
		if sub == nil {
			continue
		}
		elemental.ResetDefaultForZeroValues(sub)
		if err := sub.Validate(); err != nil {
			errors = errors.Append(err)
		}
	}

	if len(requiredErrors) > 0 {
		return requiredErrors
	}

	if len(errors) > 0 {
		return errors
	}

	return nil
}

// SpecificationForAttribute returns the AttributeSpecification for the given attribute name key.
func (*Pizza) SpecificationForAttribute(name string) elemental.AttributeSpecification {

	if v, ok := PizzaAttributesMap[name]; ok {
		return v
	}

	// We could not find it, so let's check on the lower case indexed spec map
	return PizzaLowerCaseAttributesMap[name]
}

// AttributeSpecifications returns the full attribute specifications map.
func (*Pizza) AttributeSpecifications() map[string]elemental.AttributeSpecification {

	return PizzaAttributesMap
}

// ValueForAttribute returns the value for the given attribute.
// This is a very advanced function that you should not need but in some
// very specific use cases.
func (o *Pizza) ValueForAttribute(name string) interface{} {

	switch name {
	case "ID":
		return o.ID
	case "description":
		return o.Description
	case "dough":
		return o.Dough
	case "name":
		return o.Name
	case "sauce":
		return o.Sauce
	case "toppings":
		return o.Toppings
	}

	return nil
}

// PizzaAttributesMap represents the map of attribute for Pizza.
var PizzaAttributesMap = map[string]elemental.AttributeSpecification{
	"ID": {
		AllowedChoices: []string{},
		Autogenerated:  true,
		BSONFieldName:  "_id",
		ConvertedName:  "ID",
		Description:    `ID is the identifier of the object.`,
		Exposed:        true,
		Filterable:     true,
		Identifier:     true,
		Name:           "ID",
		Orderable:      true,
		ReadOnly:       true,
		Stored:         true,
		Type:           "string",
	},
	"Description": {
		AllowedChoices: []string{},
		BSONFieldName:  "description",
		ConvertedName:  "Description",
		Description:    `The description of the pizza.`,
		Exposed:        true,
		Name:           "description",
		Stored:         true,
		Type:           "string",
	},
	"Dough": {
		AllowedChoices: []string{},
		BSONFieldName:  "dough",
		ConvertedName:  "Dough",
		Description:    `The dough of the pizza.`,
		Exposed:        true,
		Name:           "dough",
		Stored:         true,
		SubType:        "dough",
		Type:           "ref",
	},
	"Name": {
		AllowedChoices: []string{},
		BSONFieldName:  "name",
		ConvertedName:  "Name",
		Description:    `The name of the pizza.`,
		Exposed:        true,
		Name:           "name",
		Required:       true,
		Stored:         true,
		Type:           "string",
	},
	"Sauce": {
		AllowedChoices: []string{},
		BSONFieldName:  "sauce",
		ConvertedName:  "Sauce",
		Description:    `The sauce of the pizza.`,
		Exposed:        true,
		Name:           "sauce",
		Stored:         true,
		SubType:        "sauce",
		Type:           "ref",
	},
	"Toppings": {
		AllowedChoices: []string{},
		BSONFieldName:  "toppings",
		ConvertedName:  "Toppings",
		Description:    `Toppings on the pizza.`,
		Exposed:        true,
		Name:           "toppings",
		Stored:         true,
		SubType:        "topping",
		Type:           "refList",
	},
}

// PizzaLowerCaseAttributesMap represents the map of attribute for Pizza.
var PizzaLowerCaseAttributesMap = map[string]elemental.AttributeSpecification{
	"id": {
		AllowedChoices: []string{},
		Autogenerated:  true,
		BSONFieldName:  "_id",
		ConvertedName:  "ID",
		Description:    `ID is the identifier of the object.`,
		Exposed:        true,
		Filterable:     true,
		Identifier:     true,
		Name:           "ID",
		Orderable:      true,
		ReadOnly:       true,
		Stored:         true,
		Type:           "string",
	},
	"description": {
		AllowedChoices: []string{},
		BSONFieldName:  "description",
		ConvertedName:  "Description",
		Description:    `The description of the pizza.`,
		Exposed:        true,
		Name:           "description",
		Stored:         true,
		Type:           "string",
	},
	"dough": {
		AllowedChoices: []string{},
		BSONFieldName:  "dough",
		ConvertedName:  "Dough",
		Description:    `The dough of the pizza.`,
		Exposed:        true,
		Name:           "dough",
		Stored:         true,
		SubType:        "dough",
		Type:           "ref",
	},
	"name": {
		AllowedChoices: []string{},
		BSONFieldName:  "name",
		ConvertedName:  "Name",
		Description:    `The name of the pizza.`,
		Exposed:        true,
		Name:           "name",
		Required:       true,
		Stored:         true,
		Type:           "string",
	},
	"sauce": {
		AllowedChoices: []string{},
		BSONFieldName:  "sauce",
		ConvertedName:  "Sauce",
		Description:    `The sauce of the pizza.`,
		Exposed:        true,
		Name:           "sauce",
		Stored:         true,
		SubType:        "sauce",
		Type:           "ref",
	},
	"toppings": {
		AllowedChoices: []string{},
		BSONFieldName:  "toppings",
		ConvertedName:  "Toppings",
		Description:    `Toppings on the pizza.`,
		Exposed:        true,
		Name:           "toppings",
		Stored:         true,
		SubType:        "topping",
		Type:           "refList",
	},
}

// SparsePizzasList represents a list of SparsePizzas
type SparsePizzasList []*SparsePizza

// Identity returns the identity of the objects in the list.
func (o SparsePizzasList) Identity() elemental.Identity {

	return PizzaIdentity
}

// Copy returns a pointer to a copy the SparsePizzasList.
func (o SparsePizzasList) Copy() elemental.Identifiables {

	copy := append(SparsePizzasList{}, o...)
	return &copy
}

// Append appends the objects to the a new copy of the SparsePizzasList.
func (o SparsePizzasList) Append(objects ...elemental.Identifiable) elemental.Identifiables {

	out := append(SparsePizzasList{}, o...)
	for _, obj := range objects {
		out = append(out, obj.(*SparsePizza))
	}

	return out
}

// List converts the object to an elemental.IdentifiablesList.
func (o SparsePizzasList) List() elemental.IdentifiablesList {

	out := make(elemental.IdentifiablesList, len(o))
	for i := 0; i < len(o); i++ {
		out[i] = o[i]
	}

	return out
}

// DefaultOrder returns the default ordering fields of the content.
func (o SparsePizzasList) DefaultOrder() []string {

	return []string{}
}

// ToPlain returns the SparsePizzasList converted to PizzasList.
func (o SparsePizzasList) ToPlain() elemental.IdentifiablesList {

	out := make(elemental.IdentifiablesList, len(o))
	for i := 0; i < len(o); i++ {
		out[i] = o[i].ToPlain()
	}

	return out
}

// Version returns the version of the content.
func (o SparsePizzasList) Version() int {

	return 1
}

// SparsePizza represents the sparse version of a pizza.
type SparsePizza struct {
	// ID is the identifier of the object.
	ID *string `json:"ID,omitempty" msgpack:"ID,omitempty" bson:"-" mapstructure:"ID,omitempty"`

	// The description of the pizza.
	Description *string `json:"description,omitempty" msgpack:"description,omitempty" bson:"description,omitempty" mapstructure:"description,omitempty"`

	// The dough of the pizza.
	Dough *Dough `json:"dough,omitempty" msgpack:"dough,omitempty" bson:"dough,omitempty" mapstructure:"dough,omitempty"`

	// The name of the pizza.
	Name *string `json:"name,omitempty" msgpack:"name,omitempty" bson:"name,omitempty" mapstructure:"name,omitempty"`

	// The sauce of the pizza.
	Sauce *Sauce `json:"sauce,omitempty" msgpack:"sauce,omitempty" bson:"sauce,omitempty" mapstructure:"sauce,omitempty"`

	// Toppings on the pizza.
	Toppings *ToppingsList `json:"toppings,omitempty" msgpack:"toppings,omitempty" bson:"toppings,omitempty" mapstructure:"toppings,omitempty"`

	ModelVersion int `json:"-" msgpack:"-" bson:"_modelversion"`
}

// NewSparsePizza returns a new  SparsePizza.
func NewSparsePizza() *SparsePizza {
	return &SparsePizza{}
}

// Identity returns the Identity of the sparse object.
func (o *SparsePizza) Identity() elemental.Identity {

	return PizzaIdentity
}

// Identifier returns the value of the sparse object's unique identifier.
func (o *SparsePizza) Identifier() string {

	if o.ID == nil {
		return ""
	}
	return *o.ID
}

// SetIdentifier sets the value of the sparse object's unique identifier.
func (o *SparsePizza) SetIdentifier(id string) {

	if id != "" {
		o.ID = &id
	} else {
		o.ID = nil
	}
}

// GetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *SparsePizza) GetBSON() (interface{}, error) {

	if o == nil {
		return nil, nil
	}

	s := &mongoAttributesSparsePizza{}

	if o.ID != nil {
		s.ID = bson.ObjectIdHex(*o.ID)
	}
	if o.Description != nil {
		s.Description = o.Description
	}
	if o.Dough != nil {
		s.Dough = o.Dough
	}
	if o.Name != nil {
		s.Name = o.Name
	}
	if o.Sauce != nil {
		s.Sauce = o.Sauce
	}
	if o.Toppings != nil {
		s.Toppings = o.Toppings
	}

	return s, nil
}

// SetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *SparsePizza) SetBSON(raw bson.Raw) error {

	if o == nil {
		return nil
	}

	s := &mongoAttributesSparsePizza{}
	if err := raw.Unmarshal(s); err != nil {
		return err
	}

	id := s.ID.Hex()
	o.ID = &id
	if s.Description != nil {
		o.Description = s.Description
	}
	if s.Dough != nil {
		o.Dough = s.Dough
	}
	if s.Name != nil {
		o.Name = s.Name
	}
	if s.Sauce != nil {
		o.Sauce = s.Sauce
	}
	if s.Toppings != nil {
		o.Toppings = s.Toppings
	}

	return nil
}

// Version returns the hardcoded version of the model.
func (o *SparsePizza) Version() int {

	return 1
}

// ToPlain returns the plain version of the sparse model.
func (o *SparsePizza) ToPlain() elemental.PlainIdentifiable {

	out := NewPizza()
	if o.ID != nil {
		out.ID = *o.ID
	}
	if o.Description != nil {
		out.Description = *o.Description
	}
	if o.Dough != nil {
		out.Dough = o.Dough
	}
	if o.Name != nil {
		out.Name = *o.Name
	}
	if o.Sauce != nil {
		out.Sauce = o.Sauce
	}
	if o.Toppings != nil {
		out.Toppings = *o.Toppings
	}

	return out
}

// DeepCopy returns a deep copy if the SparsePizza.
func (o *SparsePizza) DeepCopy() *SparsePizza {

	if o == nil {
		return nil
	}

	out := &SparsePizza{}
	o.DeepCopyInto(out)

	return out
}

// DeepCopyInto copies the receiver into the given *SparsePizza.
func (o *SparsePizza) DeepCopyInto(out *SparsePizza) {

	target, err := copystructure.Copy(o)
	if err != nil {
		panic(fmt.Sprintf("Unable to deepcopy SparsePizza: %s", err))
	}

	*out = *target.(*SparsePizza)
}

type mongoAttributesPizza struct {
	ID          bson.ObjectId `bson:"_id,omitempty"`
	Description string        `bson:"description"`
	Dough       *Dough        `bson:"dough"`
	Name        string        `bson:"name"`
	Sauce       *Sauce        `bson:"sauce"`
	Toppings    ToppingsList  `bson:"toppings"`
}
type mongoAttributesSparsePizza struct {
	ID          bson.ObjectId `bson:"_id,omitempty"`
	Description *string       `bson:"description,omitempty"`
	Dough       *Dough        `bson:"dough,omitempty"`
	Name        *string       `bson:"name,omitempty"`
	Sauce       *Sauce        `bson:"sauce,omitempty"`
	Toppings    *ToppingsList `bson:"toppings,omitempty"`
}
