package models

import (
	"fmt"

	"github.com/globalsign/mgo/bson"
	"github.com/mitchellh/copystructure"
	"go.aporeto.io/elemental"
)

// PizzaQuantity represents the model of a pizzaquantity
type PizzaQuantity struct {
	// Identifier of the pizza.
	PizzaID string `json:"pizzaID" msgpack:"pizzaID" bson:"pizzaid" mapstructure:"pizzaID,omitempty"`

	// Quantity of pizza to order.
	Quantity int `json:"quantity" msgpack:"quantity" bson:"quantity" mapstructure:"quantity,omitempty"`

	ModelVersion int `json:"-" msgpack:"-" bson:"_modelversion"`
}

// NewPizzaQuantity returns a new *PizzaQuantity
func NewPizzaQuantity() *PizzaQuantity {

	return &PizzaQuantity{
		ModelVersion: 1,
		Quantity:     0,
	}
}

// GetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *PizzaQuantity) GetBSON() (interface{}, error) {

	if o == nil {
		return nil, nil
	}

	s := &mongoAttributesPizzaQuantity{}

	s.PizzaID = o.PizzaID
	s.Quantity = o.Quantity

	return s, nil
}

// SetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *PizzaQuantity) SetBSON(raw bson.Raw) error {

	if o == nil {
		return nil
	}

	s := &mongoAttributesPizzaQuantity{}
	if err := raw.Unmarshal(s); err != nil {
		return err
	}

	o.PizzaID = s.PizzaID
	o.Quantity = s.Quantity

	return nil
}

// BleveType implements the bleve.Classifier Interface.
func (o *PizzaQuantity) BleveType() string {

	return "pizzaquantity"
}

// DeepCopy returns a deep copy if the PizzaQuantity.
func (o *PizzaQuantity) DeepCopy() *PizzaQuantity {

	if o == nil {
		return nil
	}

	out := &PizzaQuantity{}
	o.DeepCopyInto(out)

	return out
}

// DeepCopyInto copies the receiver into the given *PizzaQuantity.
func (o *PizzaQuantity) DeepCopyInto(out *PizzaQuantity) {

	target, err := copystructure.Copy(o)
	if err != nil {
		panic(fmt.Sprintf("Unable to deepcopy PizzaQuantity: %s", err))
	}

	*out = *target.(*PizzaQuantity)
}

// Validate valides the current information stored into the structure.
func (o *PizzaQuantity) Validate() error {

	errors := elemental.Errors{}
	requiredErrors := elemental.Errors{}

	if err := elemental.ValidateRequiredString("pizzaID", o.PizzaID); err != nil {
		requiredErrors = requiredErrors.Append(err)
	}

	if err := elemental.ValidateRequiredInt("quantity", o.Quantity); err != nil {
		requiredErrors = requiredErrors.Append(err)
	}

	if len(requiredErrors) > 0 {
		return requiredErrors
	}

	if len(errors) > 0 {
		return errors
	}

	return nil
}

type mongoAttributesPizzaQuantity struct {
	PizzaID  string `bson:"pizzaid"`
	Quantity int    `bson:"quantity"`
}
