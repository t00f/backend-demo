package models

import (
	"fmt"

	"github.com/globalsign/mgo/bson"
	"github.com/mitchellh/copystructure"
	"go.aporeto.io/elemental"
)

// SauceTypeValue represents the possible values for attribute "type".
type SauceTypeValue string

const (
	// SauceTypeCustom represents the value Custom.
	SauceTypeCustom SauceTypeValue = "Custom"

	// SauceTypeRed represents the value Red.
	SauceTypeRed SauceTypeValue = "Red"

	// SauceTypeWhite represents the value White.
	SauceTypeWhite SauceTypeValue = "White"
)

// SauceIdentity represents the Identity of the object.
var SauceIdentity = elemental.Identity{
	Name:     "sauce",
	Category: "sauces",
	Package:  "default",
	Private:  false,
}

// SaucesList represents a list of Sauces
type SaucesList []*Sauce

// Identity returns the identity of the objects in the list.
func (o SaucesList) Identity() elemental.Identity {

	return SauceIdentity
}

// Copy returns a pointer to a copy the SaucesList.
func (o SaucesList) Copy() elemental.Identifiables {

	copy := append(SaucesList{}, o...)
	return &copy
}

// Append appends the objects to the a new copy of the SaucesList.
func (o SaucesList) Append(objects ...elemental.Identifiable) elemental.Identifiables {

	out := append(SaucesList{}, o...)
	for _, obj := range objects {
		out = append(out, obj.(*Sauce))
	}

	return out
}

// List converts the object to an elemental.IdentifiablesList.
func (o SaucesList) List() elemental.IdentifiablesList {

	out := make(elemental.IdentifiablesList, len(o))
	for i := 0; i < len(o); i++ {
		out[i] = o[i]
	}

	return out
}

// DefaultOrder returns the default ordering fields of the content.
func (o SaucesList) DefaultOrder() []string {

	return []string{}
}

// ToSparse returns the SaucesList converted to SparseSaucesList.
// Objects in the list will only contain the given fields. No field means entire field set.
func (o SaucesList) ToSparse(fields ...string) elemental.Identifiables {

	out := make(SparseSaucesList, len(o))
	for i := 0; i < len(o); i++ {
		out[i] = o[i].ToSparse(fields...).(*SparseSauce)
	}

	return out
}

// Version returns the version of the content.
func (o SaucesList) Version() int {

	return 1
}

// Sauce represents the model of a sauce
type Sauce struct {
	// ID is the identifier of the object.
	ID string `json:"ID" msgpack:"ID" bson:"-" mapstructure:"ID,omitempty"`

	// The description of the sauce.
	Description string `json:"description" msgpack:"description" bson:"description" mapstructure:"description,omitempty"`

	// The name of the sauce.
	Name string `json:"name" msgpack:"name" bson:"name" mapstructure:"name,omitempty"`

	// Indicates the spiceness is vegan.
	Type SauceTypeValue `json:"type" msgpack:"type" bson:"type" mapstructure:"type,omitempty"`

	// Indicates the sauce is vegan.
	Vegan bool `json:"vegan" msgpack:"vegan" bson:"vegan" mapstructure:"vegan,omitempty"`

	ModelVersion int `json:"-" msgpack:"-" bson:"_modelversion"`
}

// NewSauce returns a new *Sauce
func NewSauce() *Sauce {

	return &Sauce{
		ModelVersion: 1,
		Type:         SauceTypeRed,
	}
}

// Identity returns the Identity of the object.
func (o *Sauce) Identity() elemental.Identity {

	return SauceIdentity
}

// Identifier returns the value of the object's unique identifier.
func (o *Sauce) Identifier() string {

	return o.ID
}

// SetIdentifier sets the value of the object's unique identifier.
func (o *Sauce) SetIdentifier(id string) {

	o.ID = id
}

// GetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *Sauce) GetBSON() (interface{}, error) {

	if o == nil {
		return nil, nil
	}

	s := &mongoAttributesSauce{}

	if o.ID != "" {
		s.ID = bson.ObjectIdHex(o.ID)
	}
	s.Description = o.Description
	s.Name = o.Name
	s.Type = o.Type
	s.Vegan = o.Vegan

	return s, nil
}

// SetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *Sauce) SetBSON(raw bson.Raw) error {

	if o == nil {
		return nil
	}

	s := &mongoAttributesSauce{}
	if err := raw.Unmarshal(s); err != nil {
		return err
	}

	o.ID = s.ID.Hex()
	o.Description = s.Description
	o.Name = s.Name
	o.Type = s.Type
	o.Vegan = s.Vegan

	return nil
}

// Version returns the hardcoded version of the model.
func (o *Sauce) Version() int {

	return 1
}

// BleveType implements the bleve.Classifier Interface.
func (o *Sauce) BleveType() string {

	return "sauce"
}

// DefaultOrder returns the list of default ordering fields.
func (o *Sauce) DefaultOrder() []string {

	return []string{}
}

// Doc returns the documentation for the object
func (o *Sauce) Doc() string {

	return `This is a sauce.`
}

func (o *Sauce) String() string {

	return fmt.Sprintf("<%s:%s>", o.Identity().Name, o.Identifier())
}

// ToSparse returns the sparse version of the model.
// The returned object will only contain the given fields. No field means entire field set.
func (o *Sauce) ToSparse(fields ...string) elemental.SparseIdentifiable {

	if len(fields) == 0 {
		// nolint: goimports
		return &SparseSauce{
			ID:          &o.ID,
			Description: &o.Description,
			Name:        &o.Name,
			Type:        &o.Type,
			Vegan:       &o.Vegan,
		}
	}

	sp := &SparseSauce{}
	for _, f := range fields {
		switch f {
		case "ID":
			sp.ID = &(o.ID)
		case "description":
			sp.Description = &(o.Description)
		case "name":
			sp.Name = &(o.Name)
		case "type":
			sp.Type = &(o.Type)
		case "vegan":
			sp.Vegan = &(o.Vegan)
		}
	}

	return sp
}

// Patch apply the non nil value of a *SparseSauce to the object.
func (o *Sauce) Patch(sparse elemental.SparseIdentifiable) {
	if !sparse.Identity().IsEqual(o.Identity()) {
		panic("cannot patch from a parse with different identity")
	}

	so := sparse.(*SparseSauce)
	if so.ID != nil {
		o.ID = *so.ID
	}
	if so.Description != nil {
		o.Description = *so.Description
	}
	if so.Name != nil {
		o.Name = *so.Name
	}
	if so.Type != nil {
		o.Type = *so.Type
	}
	if so.Vegan != nil {
		o.Vegan = *so.Vegan
	}
}

// DeepCopy returns a deep copy if the Sauce.
func (o *Sauce) DeepCopy() *Sauce {

	if o == nil {
		return nil
	}

	out := &Sauce{}
	o.DeepCopyInto(out)

	return out
}

// DeepCopyInto copies the receiver into the given *Sauce.
func (o *Sauce) DeepCopyInto(out *Sauce) {

	target, err := copystructure.Copy(o)
	if err != nil {
		panic(fmt.Sprintf("Unable to deepcopy Sauce: %s", err))
	}

	*out = *target.(*Sauce)
}

// Validate valides the current information stored into the structure.
func (o *Sauce) Validate() error {

	errors := elemental.Errors{}
	requiredErrors := elemental.Errors{}

	if err := elemental.ValidateStringInList("type", string(o.Type), []string{"Red", "White", "Custom"}, false); err != nil {
		errors = errors.Append(err)
	}

	if len(requiredErrors) > 0 {
		return requiredErrors
	}

	if len(errors) > 0 {
		return errors
	}

	return nil
}

// SpecificationForAttribute returns the AttributeSpecification for the given attribute name key.
func (*Sauce) SpecificationForAttribute(name string) elemental.AttributeSpecification {

	if v, ok := SauceAttributesMap[name]; ok {
		return v
	}

	// We could not find it, so let's check on the lower case indexed spec map
	return SauceLowerCaseAttributesMap[name]
}

// AttributeSpecifications returns the full attribute specifications map.
func (*Sauce) AttributeSpecifications() map[string]elemental.AttributeSpecification {

	return SauceAttributesMap
}

// ValueForAttribute returns the value for the given attribute.
// This is a very advanced function that you should not need but in some
// very specific use cases.
func (o *Sauce) ValueForAttribute(name string) interface{} {

	switch name {
	case "ID":
		return o.ID
	case "description":
		return o.Description
	case "name":
		return o.Name
	case "type":
		return o.Type
	case "vegan":
		return o.Vegan
	}

	return nil
}

// SauceAttributesMap represents the map of attribute for Sauce.
var SauceAttributesMap = map[string]elemental.AttributeSpecification{
	"ID": {
		AllowedChoices: []string{},
		Autogenerated:  true,
		BSONFieldName:  "_id",
		ConvertedName:  "ID",
		Description:    `ID is the identifier of the object.`,
		Exposed:        true,
		Filterable:     true,
		Identifier:     true,
		Name:           "ID",
		Orderable:      true,
		ReadOnly:       true,
		Stored:         true,
		Type:           "string",
	},
	"Description": {
		AllowedChoices: []string{},
		BSONFieldName:  "description",
		ConvertedName:  "Description",
		Description:    `The description of the sauce.`,
		Exposed:        true,
		Name:           "description",
		Stored:         true,
		Type:           "string",
	},
	"Name": {
		AllowedChoices: []string{},
		BSONFieldName:  "name",
		ConvertedName:  "Name",
		Description:    `The name of the sauce.`,
		Exposed:        true,
		Name:           "name",
		Stored:         true,
		Type:           "string",
	},
	"Type": {
		AllowedChoices: []string{"Red", "White", "Custom"},
		BSONFieldName:  "type",
		ConvertedName:  "Type",
		DefaultValue:   SauceTypeRed,
		Description:    `Indicates the spiceness is vegan.`,
		Exposed:        true,
		Name:           "type",
		Stored:         true,
		Type:           "enum",
	},
	"Vegan": {
		AllowedChoices: []string{},
		BSONFieldName:  "vegan",
		ConvertedName:  "Vegan",
		Description:    `Indicates the sauce is vegan.`,
		Exposed:        true,
		Name:           "vegan",
		Stored:         true,
		Type:           "boolean",
	},
}

// SauceLowerCaseAttributesMap represents the map of attribute for Sauce.
var SauceLowerCaseAttributesMap = map[string]elemental.AttributeSpecification{
	"id": {
		AllowedChoices: []string{},
		Autogenerated:  true,
		BSONFieldName:  "_id",
		ConvertedName:  "ID",
		Description:    `ID is the identifier of the object.`,
		Exposed:        true,
		Filterable:     true,
		Identifier:     true,
		Name:           "ID",
		Orderable:      true,
		ReadOnly:       true,
		Stored:         true,
		Type:           "string",
	},
	"description": {
		AllowedChoices: []string{},
		BSONFieldName:  "description",
		ConvertedName:  "Description",
		Description:    `The description of the sauce.`,
		Exposed:        true,
		Name:           "description",
		Stored:         true,
		Type:           "string",
	},
	"name": {
		AllowedChoices: []string{},
		BSONFieldName:  "name",
		ConvertedName:  "Name",
		Description:    `The name of the sauce.`,
		Exposed:        true,
		Name:           "name",
		Stored:         true,
		Type:           "string",
	},
	"type": {
		AllowedChoices: []string{"Red", "White", "Custom"},
		BSONFieldName:  "type",
		ConvertedName:  "Type",
		DefaultValue:   SauceTypeRed,
		Description:    `Indicates the spiceness is vegan.`,
		Exposed:        true,
		Name:           "type",
		Stored:         true,
		Type:           "enum",
	},
	"vegan": {
		AllowedChoices: []string{},
		BSONFieldName:  "vegan",
		ConvertedName:  "Vegan",
		Description:    `Indicates the sauce is vegan.`,
		Exposed:        true,
		Name:           "vegan",
		Stored:         true,
		Type:           "boolean",
	},
}

// SparseSaucesList represents a list of SparseSauces
type SparseSaucesList []*SparseSauce

// Identity returns the identity of the objects in the list.
func (o SparseSaucesList) Identity() elemental.Identity {

	return SauceIdentity
}

// Copy returns a pointer to a copy the SparseSaucesList.
func (o SparseSaucesList) Copy() elemental.Identifiables {

	copy := append(SparseSaucesList{}, o...)
	return &copy
}

// Append appends the objects to the a new copy of the SparseSaucesList.
func (o SparseSaucesList) Append(objects ...elemental.Identifiable) elemental.Identifiables {

	out := append(SparseSaucesList{}, o...)
	for _, obj := range objects {
		out = append(out, obj.(*SparseSauce))
	}

	return out
}

// List converts the object to an elemental.IdentifiablesList.
func (o SparseSaucesList) List() elemental.IdentifiablesList {

	out := make(elemental.IdentifiablesList, len(o))
	for i := 0; i < len(o); i++ {
		out[i] = o[i]
	}

	return out
}

// DefaultOrder returns the default ordering fields of the content.
func (o SparseSaucesList) DefaultOrder() []string {

	return []string{}
}

// ToPlain returns the SparseSaucesList converted to SaucesList.
func (o SparseSaucesList) ToPlain() elemental.IdentifiablesList {

	out := make(elemental.IdentifiablesList, len(o))
	for i := 0; i < len(o); i++ {
		out[i] = o[i].ToPlain()
	}

	return out
}

// Version returns the version of the content.
func (o SparseSaucesList) Version() int {

	return 1
}

// SparseSauce represents the sparse version of a sauce.
type SparseSauce struct {
	// ID is the identifier of the object.
	ID *string `json:"ID,omitempty" msgpack:"ID,omitempty" bson:"-" mapstructure:"ID,omitempty"`

	// The description of the sauce.
	Description *string `json:"description,omitempty" msgpack:"description,omitempty" bson:"description,omitempty" mapstructure:"description,omitempty"`

	// The name of the sauce.
	Name *string `json:"name,omitempty" msgpack:"name,omitempty" bson:"name,omitempty" mapstructure:"name,omitempty"`

	// Indicates the spiceness is vegan.
	Type *SauceTypeValue `json:"type,omitempty" msgpack:"type,omitempty" bson:"type,omitempty" mapstructure:"type,omitempty"`

	// Indicates the sauce is vegan.
	Vegan *bool `json:"vegan,omitempty" msgpack:"vegan,omitempty" bson:"vegan,omitempty" mapstructure:"vegan,omitempty"`

	ModelVersion int `json:"-" msgpack:"-" bson:"_modelversion"`
}

// NewSparseSauce returns a new  SparseSauce.
func NewSparseSauce() *SparseSauce {
	return &SparseSauce{}
}

// Identity returns the Identity of the sparse object.
func (o *SparseSauce) Identity() elemental.Identity {

	return SauceIdentity
}

// Identifier returns the value of the sparse object's unique identifier.
func (o *SparseSauce) Identifier() string {

	if o.ID == nil {
		return ""
	}
	return *o.ID
}

// SetIdentifier sets the value of the sparse object's unique identifier.
func (o *SparseSauce) SetIdentifier(id string) {

	if id != "" {
		o.ID = &id
	} else {
		o.ID = nil
	}
}

// GetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *SparseSauce) GetBSON() (interface{}, error) {

	if o == nil {
		return nil, nil
	}

	s := &mongoAttributesSparseSauce{}

	if o.ID != nil {
		s.ID = bson.ObjectIdHex(*o.ID)
	}
	if o.Description != nil {
		s.Description = o.Description
	}
	if o.Name != nil {
		s.Name = o.Name
	}
	if o.Type != nil {
		s.Type = o.Type
	}
	if o.Vegan != nil {
		s.Vegan = o.Vegan
	}

	return s, nil
}

// SetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *SparseSauce) SetBSON(raw bson.Raw) error {

	if o == nil {
		return nil
	}

	s := &mongoAttributesSparseSauce{}
	if err := raw.Unmarshal(s); err != nil {
		return err
	}

	id := s.ID.Hex()
	o.ID = &id
	if s.Description != nil {
		o.Description = s.Description
	}
	if s.Name != nil {
		o.Name = s.Name
	}
	if s.Type != nil {
		o.Type = s.Type
	}
	if s.Vegan != nil {
		o.Vegan = s.Vegan
	}

	return nil
}

// Version returns the hardcoded version of the model.
func (o *SparseSauce) Version() int {

	return 1
}

// ToPlain returns the plain version of the sparse model.
func (o *SparseSauce) ToPlain() elemental.PlainIdentifiable {

	out := NewSauce()
	if o.ID != nil {
		out.ID = *o.ID
	}
	if o.Description != nil {
		out.Description = *o.Description
	}
	if o.Name != nil {
		out.Name = *o.Name
	}
	if o.Type != nil {
		out.Type = *o.Type
	}
	if o.Vegan != nil {
		out.Vegan = *o.Vegan
	}

	return out
}

// DeepCopy returns a deep copy if the SparseSauce.
func (o *SparseSauce) DeepCopy() *SparseSauce {

	if o == nil {
		return nil
	}

	out := &SparseSauce{}
	o.DeepCopyInto(out)

	return out
}

// DeepCopyInto copies the receiver into the given *SparseSauce.
func (o *SparseSauce) DeepCopyInto(out *SparseSauce) {

	target, err := copystructure.Copy(o)
	if err != nil {
		panic(fmt.Sprintf("Unable to deepcopy SparseSauce: %s", err))
	}

	*out = *target.(*SparseSauce)
}

type mongoAttributesSauce struct {
	ID          bson.ObjectId  `bson:"_id,omitempty"`
	Description string         `bson:"description"`
	Name        string         `bson:"name"`
	Type        SauceTypeValue `bson:"type"`
	Vegan       bool           `bson:"vegan"`
}
type mongoAttributesSparseSauce struct {
	ID          bson.ObjectId   `bson:"_id,omitempty"`
	Description *string         `bson:"description,omitempty"`
	Name        *string         `bson:"name,omitempty"`
	Type        *SauceTypeValue `bson:"type,omitempty"`
	Vegan       *bool           `bson:"vegan,omitempty"`
}
