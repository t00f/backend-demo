# Model
model:
  rest_name: orderitem
  resource_name: orderitems
  entity_name: OrderItem
  package: core
  group: core
  description: This represents the quantity of a pizza to order.
  extends:
  - '@identifiable'
  detached: true

# Attributes
attributes:
  v1:
  - name: itemType
    description: Type of item. For now, we're only doing pizza.
    type: enum
    exposed: true
    stored: true
    required: true
    allowed_choices:
    - Pizza
    default_value: Pizza

  - name: quantity
    description: Quantity of item to order.
    type: integer
    exposed: true
    stored: true
    required: true
    default_value: 0
