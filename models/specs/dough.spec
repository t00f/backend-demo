# Model
model:
  rest_name: dough
  resource_name: doughs
  entity_name: Dough
  package: default
  group: core
  description: This is a dough.
  get:
    description: Gets the dough.
  update:
    description: Updates the dough.
  delete:
    description: Deletes the dough.
  extends:
  - '@identifiable'

# Attributes
attributes:
  v1:
  - name: description
    description: The description of the dough.
    type: string
    exposed: true
    stored: true

  - name: name
    description: The name of the dough.
    type: string
    exposed: true
    stored: true

  - name: style
    description: The style of the dough.
    type: enum
    exposed: true
    stored: true
    allowed_choices:
    - NewYork
    - Napolitan
    - Sicilian
    - Cheesecrust
    default_value: NewYork
