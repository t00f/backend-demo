# Model
model:
  rest_name: order
  resource_name: orders
  entity_name: Order
  package: core
  group: core
  description: This is an order.
  get:
    description: Retrieve a order given its identifier.
  update:
    description: Update a order given its identifier.
  delete:
    description: Delete a orderge given its identifier.
  extends:
  - '@identifiable'
  validations:
  - $validateOrder

# Attributes
attributes:
  v1:
  - name: items
    description: List of items of the order.
    type: refList
    exposed: true
    subtype: orderitem
    stored: true
    extensions:
      refMode: pointer
