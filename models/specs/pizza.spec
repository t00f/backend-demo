# Model
model:
  rest_name: pizza
  resource_name: pizzas
  entity_name: Pizza
  package: core
  group: core
  description: This is a Pizza.
  get:
    description: Retrieve a pizza given its identifier.
  update:
    description: Update a pizza given its identifier.
  delete:
    description: Delete a pizza given its identifier.
  extends:
  - '@identifiable'

# Attributes
attributes:
  v1:
  - name: description
    description: The description of the pizza.
    type: string
    exposed: true
    stored: true

  - name: dough
    description: The dough of the pizza.
    type: ref
    exposed: true
    subtype: dough
    stored: true
    extensions:
      noInit: true
      refMode: pointer

  - name: name
    description: The name of the pizza.
    type: string
    exposed: true
    stored: true
    required: true
    example_value: calzone
    validations:
    - $validatePizzaName

  - name: sauce
    description: The sauce of the pizza.
    type: ref
    exposed: true
    subtype: sauce
    stored: true
    extensions:
      noInit: true
      refMode: pointer

  - name: toppings
    description: Toppings on the pizza.
    type: refList
    exposed: true
    subtype: topping
    stored: true
