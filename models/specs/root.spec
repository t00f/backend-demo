# Model
model:
  rest_name: root
  resource_name: root
  entity_name: Root
  package: root
  group: core
  description: This model represents the / endpoint.
  root: true

# Relations
relations:
- rest_name: dough
  get:
    description: List all dough available.
    global_parameters:
    - $queryable
  create:
    description: Create a new dough.

- rest_name: order
  get:
    description: List all orders available.
  create:
    description: Create a new order.

- rest_name: pizza
  get:
    description: List all pizza available.
    global_parameters:
    - $queryable
  create:
    description: Create a new pizza.

- rest_name: sauce
  get:
    description: List all sauce available.
    global_parameters:
    - $queryable
  create:
    description: Create a new sauce.

- rest_name: topping
  get:
    description: List all topping available.
    global_parameters:
    - $queryable
  create:
    description: Create a new topping.
