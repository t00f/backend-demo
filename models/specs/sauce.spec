# Model
model:
  rest_name: sauce
  resource_name: sauces
  entity_name: Sauce
  package: default
  group: core
  description: This is a sauce.
  get:
    description: Gets the sauce.
  update:
    description: Updates the sauce.
  delete:
    description: Deletes the sauce.
  extends:
  - '@identifiable'

# Attributes
attributes:
  v1:
  - name: description
    description: The description of the sauce.
    type: string
    exposed: true
    stored: true

  - name: name
    description: The name of the sauce.
    type: string
    exposed: true
    stored: true

  - name: type
    description: Indicates the spiceness is vegan.
    type: enum
    exposed: true
    stored: true
    allowed_choices:
    - Red
    - White
    - Custom
    default_value: Red

  - name: vegan
    description: Indicates the sauce is vegan.
    type: boolean
    exposed: true
    stored: true
