# Model
model:
  rest_name: topping
  resource_name: toppings
  entity_name: Topping
  package: default
  group: core
  description: This is a topping.
  get:
    description: Gets the topping.
  update:
    description: Updates the topping.
  delete:
    description: Deletes the topping.
  extends:
  - '@identifiable'

# Attributes
attributes:
  v1:
  - name: description
    description: The description of the topping.
    type: string
    exposed: true
    stored: true

  - name: name
    description: The name of the topping.
    type: string
    exposed: true
    stored: true

  - name: spiciness
    description: Indicates the spiceness is vegan.
    type: enum
    exposed: true
    stored: true
    allowed_choices:
    - None
    - Mild
    - Hot
    default_value: None

  - name: vegan
    description: Indicates the topping is vegan.
    type: boolean
    exposed: true
    stored: true
