package models

import (
	"fmt"

	"github.com/globalsign/mgo/bson"
	"github.com/mitchellh/copystructure"
	"go.aporeto.io/elemental"
)

// ToppingSpicinessValue represents the possible values for attribute "spiciness".
type ToppingSpicinessValue string

const (
	// ToppingSpicinessHot represents the value Hot.
	ToppingSpicinessHot ToppingSpicinessValue = "Hot"

	// ToppingSpicinessMild represents the value Mild.
	ToppingSpicinessMild ToppingSpicinessValue = "Mild"

	// ToppingSpicinessNone represents the value None.
	ToppingSpicinessNone ToppingSpicinessValue = "None"
)

// ToppingIdentity represents the Identity of the object.
var ToppingIdentity = elemental.Identity{
	Name:     "topping",
	Category: "toppings",
	Package:  "default",
	Private:  false,
}

// ToppingsList represents a list of Toppings
type ToppingsList []*Topping

// Identity returns the identity of the objects in the list.
func (o ToppingsList) Identity() elemental.Identity {

	return ToppingIdentity
}

// Copy returns a pointer to a copy the ToppingsList.
func (o ToppingsList) Copy() elemental.Identifiables {

	copy := append(ToppingsList{}, o...)
	return &copy
}

// Append appends the objects to the a new copy of the ToppingsList.
func (o ToppingsList) Append(objects ...elemental.Identifiable) elemental.Identifiables {

	out := append(ToppingsList{}, o...)
	for _, obj := range objects {
		out = append(out, obj.(*Topping))
	}

	return out
}

// List converts the object to an elemental.IdentifiablesList.
func (o ToppingsList) List() elemental.IdentifiablesList {

	out := make(elemental.IdentifiablesList, len(o))
	for i := 0; i < len(o); i++ {
		out[i] = o[i]
	}

	return out
}

// DefaultOrder returns the default ordering fields of the content.
func (o ToppingsList) DefaultOrder() []string {

	return []string{}
}

// ToSparse returns the ToppingsList converted to SparseToppingsList.
// Objects in the list will only contain the given fields. No field means entire field set.
func (o ToppingsList) ToSparse(fields ...string) elemental.Identifiables {

	out := make(SparseToppingsList, len(o))
	for i := 0; i < len(o); i++ {
		out[i] = o[i].ToSparse(fields...).(*SparseTopping)
	}

	return out
}

// Version returns the version of the content.
func (o ToppingsList) Version() int {

	return 1
}

// Topping represents the model of a topping
type Topping struct {
	// ID is the identifier of the object.
	ID string `json:"ID" msgpack:"ID" bson:"-" mapstructure:"ID,omitempty"`

	// The description of the topping.
	Description string `json:"description" msgpack:"description" bson:"description" mapstructure:"description,omitempty"`

	// The name of the topping.
	Name string `json:"name" msgpack:"name" bson:"name" mapstructure:"name,omitempty"`

	// Indicates the spiceness is vegan.
	Spiciness ToppingSpicinessValue `json:"spiciness" msgpack:"spiciness" bson:"spiciness" mapstructure:"spiciness,omitempty"`

	// Indicates the topping is vegan.
	Vegan bool `json:"vegan" msgpack:"vegan" bson:"vegan" mapstructure:"vegan,omitempty"`

	ModelVersion int `json:"-" msgpack:"-" bson:"_modelversion"`
}

// NewTopping returns a new *Topping
func NewTopping() *Topping {

	return &Topping{
		ModelVersion: 1,
		Spiciness:    ToppingSpicinessNone,
	}
}

// Identity returns the Identity of the object.
func (o *Topping) Identity() elemental.Identity {

	return ToppingIdentity
}

// Identifier returns the value of the object's unique identifier.
func (o *Topping) Identifier() string {

	return o.ID
}

// SetIdentifier sets the value of the object's unique identifier.
func (o *Topping) SetIdentifier(id string) {

	o.ID = id
}

// GetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *Topping) GetBSON() (interface{}, error) {

	if o == nil {
		return nil, nil
	}

	s := &mongoAttributesTopping{}

	if o.ID != "" {
		s.ID = bson.ObjectIdHex(o.ID)
	}
	s.Description = o.Description
	s.Name = o.Name
	s.Spiciness = o.Spiciness
	s.Vegan = o.Vegan

	return s, nil
}

// SetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *Topping) SetBSON(raw bson.Raw) error {

	if o == nil {
		return nil
	}

	s := &mongoAttributesTopping{}
	if err := raw.Unmarshal(s); err != nil {
		return err
	}

	o.ID = s.ID.Hex()
	o.Description = s.Description
	o.Name = s.Name
	o.Spiciness = s.Spiciness
	o.Vegan = s.Vegan

	return nil
}

// Version returns the hardcoded version of the model.
func (o *Topping) Version() int {

	return 1
}

// BleveType implements the bleve.Classifier Interface.
func (o *Topping) BleveType() string {

	return "topping"
}

// DefaultOrder returns the list of default ordering fields.
func (o *Topping) DefaultOrder() []string {

	return []string{}
}

// Doc returns the documentation for the object
func (o *Topping) Doc() string {

	return `This is a topping.`
}

func (o *Topping) String() string {

	return fmt.Sprintf("<%s:%s>", o.Identity().Name, o.Identifier())
}

// ToSparse returns the sparse version of the model.
// The returned object will only contain the given fields. No field means entire field set.
func (o *Topping) ToSparse(fields ...string) elemental.SparseIdentifiable {

	if len(fields) == 0 {
		// nolint: goimports
		return &SparseTopping{
			ID:          &o.ID,
			Description: &o.Description,
			Name:        &o.Name,
			Spiciness:   &o.Spiciness,
			Vegan:       &o.Vegan,
		}
	}

	sp := &SparseTopping{}
	for _, f := range fields {
		switch f {
		case "ID":
			sp.ID = &(o.ID)
		case "description":
			sp.Description = &(o.Description)
		case "name":
			sp.Name = &(o.Name)
		case "spiciness":
			sp.Spiciness = &(o.Spiciness)
		case "vegan":
			sp.Vegan = &(o.Vegan)
		}
	}

	return sp
}

// Patch apply the non nil value of a *SparseTopping to the object.
func (o *Topping) Patch(sparse elemental.SparseIdentifiable) {
	if !sparse.Identity().IsEqual(o.Identity()) {
		panic("cannot patch from a parse with different identity")
	}

	so := sparse.(*SparseTopping)
	if so.ID != nil {
		o.ID = *so.ID
	}
	if so.Description != nil {
		o.Description = *so.Description
	}
	if so.Name != nil {
		o.Name = *so.Name
	}
	if so.Spiciness != nil {
		o.Spiciness = *so.Spiciness
	}
	if so.Vegan != nil {
		o.Vegan = *so.Vegan
	}
}

// DeepCopy returns a deep copy if the Topping.
func (o *Topping) DeepCopy() *Topping {

	if o == nil {
		return nil
	}

	out := &Topping{}
	o.DeepCopyInto(out)

	return out
}

// DeepCopyInto copies the receiver into the given *Topping.
func (o *Topping) DeepCopyInto(out *Topping) {

	target, err := copystructure.Copy(o)
	if err != nil {
		panic(fmt.Sprintf("Unable to deepcopy Topping: %s", err))
	}

	*out = *target.(*Topping)
}

// Validate valides the current information stored into the structure.
func (o *Topping) Validate() error {

	errors := elemental.Errors{}
	requiredErrors := elemental.Errors{}

	if err := elemental.ValidateStringInList("spiciness", string(o.Spiciness), []string{"None", "Mild", "Hot"}, false); err != nil {
		errors = errors.Append(err)
	}

	if len(requiredErrors) > 0 {
		return requiredErrors
	}

	if len(errors) > 0 {
		return errors
	}

	return nil
}

// SpecificationForAttribute returns the AttributeSpecification for the given attribute name key.
func (*Topping) SpecificationForAttribute(name string) elemental.AttributeSpecification {

	if v, ok := ToppingAttributesMap[name]; ok {
		return v
	}

	// We could not find it, so let's check on the lower case indexed spec map
	return ToppingLowerCaseAttributesMap[name]
}

// AttributeSpecifications returns the full attribute specifications map.
func (*Topping) AttributeSpecifications() map[string]elemental.AttributeSpecification {

	return ToppingAttributesMap
}

// ValueForAttribute returns the value for the given attribute.
// This is a very advanced function that you should not need but in some
// very specific use cases.
func (o *Topping) ValueForAttribute(name string) interface{} {

	switch name {
	case "ID":
		return o.ID
	case "description":
		return o.Description
	case "name":
		return o.Name
	case "spiciness":
		return o.Spiciness
	case "vegan":
		return o.Vegan
	}

	return nil
}

// ToppingAttributesMap represents the map of attribute for Topping.
var ToppingAttributesMap = map[string]elemental.AttributeSpecification{
	"ID": {
		AllowedChoices: []string{},
		Autogenerated:  true,
		BSONFieldName:  "_id",
		ConvertedName:  "ID",
		Description:    `ID is the identifier of the object.`,
		Exposed:        true,
		Filterable:     true,
		Identifier:     true,
		Name:           "ID",
		Orderable:      true,
		ReadOnly:       true,
		Stored:         true,
		Type:           "string",
	},
	"Description": {
		AllowedChoices: []string{},
		BSONFieldName:  "description",
		ConvertedName:  "Description",
		Description:    `The description of the topping.`,
		Exposed:        true,
		Name:           "description",
		Stored:         true,
		Type:           "string",
	},
	"Name": {
		AllowedChoices: []string{},
		BSONFieldName:  "name",
		ConvertedName:  "Name",
		Description:    `The name of the topping.`,
		Exposed:        true,
		Name:           "name",
		Stored:         true,
		Type:           "string",
	},
	"Spiciness": {
		AllowedChoices: []string{"None", "Mild", "Hot"},
		BSONFieldName:  "spiciness",
		ConvertedName:  "Spiciness",
		DefaultValue:   ToppingSpicinessNone,
		Description:    `Indicates the spiceness is vegan.`,
		Exposed:        true,
		Name:           "spiciness",
		Stored:         true,
		Type:           "enum",
	},
	"Vegan": {
		AllowedChoices: []string{},
		BSONFieldName:  "vegan",
		ConvertedName:  "Vegan",
		Description:    `Indicates the topping is vegan.`,
		Exposed:        true,
		Name:           "vegan",
		Stored:         true,
		Type:           "boolean",
	},
}

// ToppingLowerCaseAttributesMap represents the map of attribute for Topping.
var ToppingLowerCaseAttributesMap = map[string]elemental.AttributeSpecification{
	"id": {
		AllowedChoices: []string{},
		Autogenerated:  true,
		BSONFieldName:  "_id",
		ConvertedName:  "ID",
		Description:    `ID is the identifier of the object.`,
		Exposed:        true,
		Filterable:     true,
		Identifier:     true,
		Name:           "ID",
		Orderable:      true,
		ReadOnly:       true,
		Stored:         true,
		Type:           "string",
	},
	"description": {
		AllowedChoices: []string{},
		BSONFieldName:  "description",
		ConvertedName:  "Description",
		Description:    `The description of the topping.`,
		Exposed:        true,
		Name:           "description",
		Stored:         true,
		Type:           "string",
	},
	"name": {
		AllowedChoices: []string{},
		BSONFieldName:  "name",
		ConvertedName:  "Name",
		Description:    `The name of the topping.`,
		Exposed:        true,
		Name:           "name",
		Stored:         true,
		Type:           "string",
	},
	"spiciness": {
		AllowedChoices: []string{"None", "Mild", "Hot"},
		BSONFieldName:  "spiciness",
		ConvertedName:  "Spiciness",
		DefaultValue:   ToppingSpicinessNone,
		Description:    `Indicates the spiceness is vegan.`,
		Exposed:        true,
		Name:           "spiciness",
		Stored:         true,
		Type:           "enum",
	},
	"vegan": {
		AllowedChoices: []string{},
		BSONFieldName:  "vegan",
		ConvertedName:  "Vegan",
		Description:    `Indicates the topping is vegan.`,
		Exposed:        true,
		Name:           "vegan",
		Stored:         true,
		Type:           "boolean",
	},
}

// SparseToppingsList represents a list of SparseToppings
type SparseToppingsList []*SparseTopping

// Identity returns the identity of the objects in the list.
func (o SparseToppingsList) Identity() elemental.Identity {

	return ToppingIdentity
}

// Copy returns a pointer to a copy the SparseToppingsList.
func (o SparseToppingsList) Copy() elemental.Identifiables {

	copy := append(SparseToppingsList{}, o...)
	return &copy
}

// Append appends the objects to the a new copy of the SparseToppingsList.
func (o SparseToppingsList) Append(objects ...elemental.Identifiable) elemental.Identifiables {

	out := append(SparseToppingsList{}, o...)
	for _, obj := range objects {
		out = append(out, obj.(*SparseTopping))
	}

	return out
}

// List converts the object to an elemental.IdentifiablesList.
func (o SparseToppingsList) List() elemental.IdentifiablesList {

	out := make(elemental.IdentifiablesList, len(o))
	for i := 0; i < len(o); i++ {
		out[i] = o[i]
	}

	return out
}

// DefaultOrder returns the default ordering fields of the content.
func (o SparseToppingsList) DefaultOrder() []string {

	return []string{}
}

// ToPlain returns the SparseToppingsList converted to ToppingsList.
func (o SparseToppingsList) ToPlain() elemental.IdentifiablesList {

	out := make(elemental.IdentifiablesList, len(o))
	for i := 0; i < len(o); i++ {
		out[i] = o[i].ToPlain()
	}

	return out
}

// Version returns the version of the content.
func (o SparseToppingsList) Version() int {

	return 1
}

// SparseTopping represents the sparse version of a topping.
type SparseTopping struct {
	// ID is the identifier of the object.
	ID *string `json:"ID,omitempty" msgpack:"ID,omitempty" bson:"-" mapstructure:"ID,omitempty"`

	// The description of the topping.
	Description *string `json:"description,omitempty" msgpack:"description,omitempty" bson:"description,omitempty" mapstructure:"description,omitempty"`

	// The name of the topping.
	Name *string `json:"name,omitempty" msgpack:"name,omitempty" bson:"name,omitempty" mapstructure:"name,omitempty"`

	// Indicates the spiceness is vegan.
	Spiciness *ToppingSpicinessValue `json:"spiciness,omitempty" msgpack:"spiciness,omitempty" bson:"spiciness,omitempty" mapstructure:"spiciness,omitempty"`

	// Indicates the topping is vegan.
	Vegan *bool `json:"vegan,omitempty" msgpack:"vegan,omitempty" bson:"vegan,omitempty" mapstructure:"vegan,omitempty"`

	ModelVersion int `json:"-" msgpack:"-" bson:"_modelversion"`
}

// NewSparseTopping returns a new  SparseTopping.
func NewSparseTopping() *SparseTopping {
	return &SparseTopping{}
}

// Identity returns the Identity of the sparse object.
func (o *SparseTopping) Identity() elemental.Identity {

	return ToppingIdentity
}

// Identifier returns the value of the sparse object's unique identifier.
func (o *SparseTopping) Identifier() string {

	if o.ID == nil {
		return ""
	}
	return *o.ID
}

// SetIdentifier sets the value of the sparse object's unique identifier.
func (o *SparseTopping) SetIdentifier(id string) {

	if id != "" {
		o.ID = &id
	} else {
		o.ID = nil
	}
}

// GetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *SparseTopping) GetBSON() (interface{}, error) {

	if o == nil {
		return nil, nil
	}

	s := &mongoAttributesSparseTopping{}

	if o.ID != nil {
		s.ID = bson.ObjectIdHex(*o.ID)
	}
	if o.Description != nil {
		s.Description = o.Description
	}
	if o.Name != nil {
		s.Name = o.Name
	}
	if o.Spiciness != nil {
		s.Spiciness = o.Spiciness
	}
	if o.Vegan != nil {
		s.Vegan = o.Vegan
	}

	return s, nil
}

// SetBSON implements the bson marshaling interface.
// This is used to transparently convert ID to MongoDBID as ObectID.
func (o *SparseTopping) SetBSON(raw bson.Raw) error {

	if o == nil {
		return nil
	}

	s := &mongoAttributesSparseTopping{}
	if err := raw.Unmarshal(s); err != nil {
		return err
	}

	id := s.ID.Hex()
	o.ID = &id
	if s.Description != nil {
		o.Description = s.Description
	}
	if s.Name != nil {
		o.Name = s.Name
	}
	if s.Spiciness != nil {
		o.Spiciness = s.Spiciness
	}
	if s.Vegan != nil {
		o.Vegan = s.Vegan
	}

	return nil
}

// Version returns the hardcoded version of the model.
func (o *SparseTopping) Version() int {

	return 1
}

// ToPlain returns the plain version of the sparse model.
func (o *SparseTopping) ToPlain() elemental.PlainIdentifiable {

	out := NewTopping()
	if o.ID != nil {
		out.ID = *o.ID
	}
	if o.Description != nil {
		out.Description = *o.Description
	}
	if o.Name != nil {
		out.Name = *o.Name
	}
	if o.Spiciness != nil {
		out.Spiciness = *o.Spiciness
	}
	if o.Vegan != nil {
		out.Vegan = *o.Vegan
	}

	return out
}

// DeepCopy returns a deep copy if the SparseTopping.
func (o *SparseTopping) DeepCopy() *SparseTopping {

	if o == nil {
		return nil
	}

	out := &SparseTopping{}
	o.DeepCopyInto(out)

	return out
}

// DeepCopyInto copies the receiver into the given *SparseTopping.
func (o *SparseTopping) DeepCopyInto(out *SparseTopping) {

	target, err := copystructure.Copy(o)
	if err != nil {
		panic(fmt.Sprintf("Unable to deepcopy SparseTopping: %s", err))
	}

	*out = *target.(*SparseTopping)
}

type mongoAttributesTopping struct {
	ID          bson.ObjectId         `bson:"_id,omitempty"`
	Description string                `bson:"description"`
	Name        string                `bson:"name"`
	Spiciness   ToppingSpicinessValue `bson:"spiciness"`
	Vegan       bool                  `bson:"vegan"`
}
type mongoAttributesSparseTopping struct {
	ID          bson.ObjectId          `bson:"_id,omitempty"`
	Description *string                `bson:"description,omitempty"`
	Name        *string                `bson:"name,omitempty"`
	Spiciness   *ToppingSpicinessValue `bson:"spiciness,omitempty"`
	Vegan       *bool                  `bson:"vegan,omitempty"`
}
