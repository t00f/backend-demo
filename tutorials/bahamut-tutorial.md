# Bahamut tutorial - PART 1



## Introduction



### Context

In this step by step tutorial, we’ll realize a complete backend in Golang using the Stack bahamut/elemental/regolithe…

I’ll cover all the details along the road, but let’s start with a simple server.

### Story

We are looking to open a shop that makes pizzas 🍕Easy!

### Requirements

* Golang
* Get the libraries

```bash
go get -u go.aporeto.io/bahamut
go get -u go.aporeto.io/manipulate
go get -u go.aporeto.io/tg
```



Start by creating a folder that will contain your backend code:

```bash
mkdir backend-demo
cd backend-demo
```



## Preparing the model

First thing we need to do is to define the model. For now, we only have one model which represents the resource `pizza` .

We are going to write a YAML specification file which will be converted into a Go file.

The conversion will be handled thanks to the following libraries

* `go.aporeto.io/regolithe` - Regolithe provides the command `rego` that allows to register new commands to manage conversions.
* `go.aporeto.io/elemental` - Elemental is the based library that any exposed resource should inherit from. It provides a commande `elegen` that provides the templates to convert a `.spec` file into a `.go` file

>  Regolithe and Elemental are well integrated with vscode. When saving the specification file, vscode uses elegen to validate the YAML file and generates the associated golang files.



Get the two commands:

```
go get -u go.aporeto.io/regolithe
go get -u go.aporeto.io/regolithe/cmd/rego
go get -u go.aporeto.io/elemental
go get -u go.aporeto.io/elemental/cmd/elegen
```



Create a models folder that will holds your specifications files and run `rego init` to generates a default structure.

```
# mkdir models && cd models
rego init models
```

The `specs`folder is generated and contains some specification definitions:

* `.regolithe-gen-cmd` indicates what to do when saving a specification file. Let's update this file to put all the generated `.go` file in the `models` folder:

```bash
#!/bin/bash
mkdir -p ../doc
data=$(rego doc -d . || exit 1)
echo "$data" > ../doc/documentation.md
cd .. || exit 1


elegen folder -d specs -o codegen || exit 1
mv codegen/elemental/*.go .
rm -rf codegen
```

* `regolithe.ini` provides a configuration to regolithe. You can update it however you prefer. For now, update the transformer name to `models`. This will generate golang file with the `package models` header.

* `_api.info` provides general information about your API

* `root.spec` represents the resource behind the `/` API
* `object.spec` is one resource. In the next step, we'll update this file to represent our pizza.
* `@identifiable.abs` is an abstract model. This is useful if you want multiple models to inherit from an abstract model. Check how the `object.spec` inherits this model to define its attribute `ID`.
* `_type.mapping` allows to defines new types to describe the attributes of a model.
* `_parameters.mapping` allows to defines the query parameters allowed in a GET api.

> If you are using the vscode extension, you can save one of the `.spec` file to generate the `.go` files. Otherwise, you can simply run the script in `.regolithe-gen-cmd`

## Defining our pizza model

Now we know we are going to need an api `GET /pizzas`. To do that we will need to create a `pizza.spec`. You can duplicate the existing `object.spec` or simply update it with the following:

```yaml
# Model
model:
  rest_name: pizza
  resource_name: pizzas
  entity_name: Pizza
  package: core
  group: core
  description: This is a Pizza.
  extends:
  - '@identifiable'

# Attributes
attributes:
  v1:
  - name: name
    description: The name of the pizza.
    type: string
    exposed: true
    stored: true
```

* Update the relations in the `root.spec` to define the route `/pizzas`. Note that we provide the `rest_name` to indicate the model, but the route is always using the `resource_name`as REST is always plural by convention

```yaml
# Model
model:
  rest_name: root
  resource_name: root
  entity_name: Root
  package: root
  group: core
  description: This model represents the / endpoint.


relations:
  - rest_name: pizza
    get:
        description: List all pizza available.
```

Save these files or run `.regolithe-gen-cmd` to re-generate the `.go` files. You should expect to see a `pizza.go` in the models folder.



## Building a basic REST server

Now that we have our model, we can create our server that will have one service. This service will process the requests coming on the `/pizzas` endpoint.



We are creating a `services` folder that will contain multiple services. For now, there is only one service called `pizza ` that will have one API endpoint `GET /pizzas` to list all the available pizzas on the menu:

```bash
mkdir -p services/pizza/processors
cd services/pizza/processors
```

* In `/services/pizza` we are going to create the bahamut server.

```go
package main

import (
	"context"

	"gitlab.com/lost_children/backend-demo/models"
	"gitlab.com/lost_children/backend-demo/services/pizza/internal/processors"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"go.aporeto.io/bahamut"
	"go.aporeto.io/elemental"
)

func main() {

	// Initialize a context
	ctx, cancel := context.WithCancel(context.Background())
	bahamut.InstallSIGINTHandler(cancel)

  // Initialize a logger
	logger, _ := zap.NewDevelopment()
	zap.ReplaceGlobals(logger.WithOptions(zap.IncreaseLevel(zapcore.DebugLevel)))

	// Initialize the server
	server := bahamut.New(
		bahamut.OptServiceInfo("pizza", "1.0", nil),
		bahamut.OptModel(map[int]elemental.ModelManager{0: models.Manager(), 1: models.Manager()}),
		bahamut.OptRestServer("127.0.0.1:8000"),
	)

	// Register the processor of pizzas for the Pizza resource.
	bahamut.RegisterProcessorOrDie(server, processors.NewPizzaProcessor(), models.PizzaIdentity)

	// Run the server
	server.Run(ctx)

	<-ctx.Done()
}
```



* In `/services/pizza/processors` we are going to create the processor that will handle requests for the model `pizza` created previously

```go
package processors

import (
	"net/http"
	"sync"

	"gitlab.com/lost_children/backend-demo/models"
	"go.aporeto.io/bahamut"
	"go.aporeto.io/elemental"
	"go.uber.org/zap"
)

// PizzaProcessor processes requests coming on /pizzas endpoint
type PizzaProcessor struct {
	lameDB        map[string]*models.Pizza
	lameDBLock    sync.RWMutex
	lameNextIndex int
}

// NewPizzaProcessor creates an instance of the Pizza processor.
func NewPizzaProcessor() *PizzaProcessor {

	return &PizzaProcessor{
		lameNextIndex: 3,
		lameDB: map[string]*models.Pizza{
			"1": {ID: "1", Name: "Meat Lover"},
			"2": {ID: "2", Name: "Margarita"},
		},
	}
}

// ProcessRetrieveMany implements the retrieve-many capability as a bahamut Processor.
func (p *PizzaProcessor) ProcessRetrieveMany(bctx bahamut.Context) error {

	zap.L().Info("received request", zap.String("op", string(bctx.Request().Operation)))

	p.lameDBLock.RLock()
	defer p.lameDBLock.RUnlock()

	out := models.PizzasList{}
	for _, p := range p.lameDB {
		out = append(out, p)
	}

	bctx.SetOutputData(out)

	return nil
}

```



## Running the server

In order to run the server, build the service and run it:

```bash
go build ./services/pizza/ && ./pizza
2020-06-25T14:59:29.924-0700    INFO    bahamut/rest_server.go:246      API server started      {"address": "127.0.0.1:8000"}
```

You should be able to make a request to get the pizzas

```bash
curl http://127.0.0.1:8000/pizzas
[{"ID":"1","name":"Meat Lover"},{"ID":"2","name":"Margarita"}]
```

As explained, this is a very basic server to introduce the first concepts:

* the elemental model concep.
* the bahamut server concept.
* the processor concept.

In the next section, we'll add everything needed for a production ready server.



# Bahamut tutorial - PART 2

Now that we have a very basic server, the first enhancement is to be able to create a pizza. For that matter, we will introduce the use of MongoDB and improve our current processor `pizza`



## Adding a real database

### Running MongoDB

Install Mongo DB by following the [official documentation](https://docs.mongodb.com/manual/administration/install-community/)

As I am working on a mac, I am installing Mongo using homebrew:

```bash
brew tap mongodb/brew
brew install mongodb-community
```

You can then run mongo with the following command:

```
mkdir -p .data/mongo
mongod --dbpath .data/mongo --bind_ip 0.0.0.0
```

> Use `mongo` command line tool to verify you can connect to your database.

MongoDB is running, we can now go back to our bahamut server.

### Enhance the pizza processor

Currently, our processor `pizza` is simple having a map to store the pizza. We are going to configure the mongo database and then create a `manipulator` to access it.

#### About Lombric

To simplify the configure, we'll use a small library called `lombric` that is capable of loading a configuration.

We want our configuration to allow some arguments:

```
./pizza -h
Usage of ./pizza:
			--listen string      Listen address (default "127.0.0.1:8000")
      --mongo-db string    Mongo database name (default "pizza")
      --mongo-url string   Mongo database address (default "mongodb://127.0.0.1:27017")
```

So we define the following structure

```go
var conf = struct {
	Listen      string `mapstructure:"listen" desc:"Listen address" default:"127.0.0.1:8000"`
	MongoURL    string `mapstructure:"mongo-url" desc:"Mongo url" default:"mongodb://127.0.0.1:27017"`
	MongoDBName string `mapstructure:"mongo-db" desc:"Mongo database name" default:"pizza"`
}{}
```

and call lombric:

```go
lombric.Initialize(&conf)
```

> Note: Lombric is based on the suite [pflag](https://github.com/spf13/pflag), [viper](https://github.com/spf13/viper) and [cobra](https://github.com/spf13/cobra). Check out these repositories if you want to create a cool CLI ;)

#### About manipulator

We can now use this configuration to define a `manipulator`. A `manipulator` is an object that allows to manipulate an `elemental` object. Do your remember our model? We created a pizza model that is elemental object!

There are different type of manipulator, but they are all coming from the library called `manipulate`:

* `manipulate.maniphttp ` allows to make HTTP requests
* `manipulate.manipmemory` allows to interact with an in-memory database
* `manipulate.maniptest` allows to mock any source
* `manipulate.manipmongo` allows to interact with a mongo database and that's what we need here!

A manipulator is an interface that provides the following methods:

```go
// Manipulator is the interface of a storage backend.
type Manipulator interface {

	// RetrieveMany retrieves the a list of objects with the given elemental.Identity and put them in the given dest.
	RetrieveMany(mctx Context, dest elemental.Identifiables) error

	// Retrieve retrieves one or multiple elemental.Identifiables.
	// In order to be retrievable, the elemental.Identifiable needs to have their Identifier correctly set.
	Retrieve(mctx Context, object elemental.Identifiable) error

	// Create creates a the given elemental.Identifiables.
	Create(mctx Context, object elemental.Identifiable) error

	// Update updates one or multiple elemental.Identifiables.
	// In order to be updatable, the elemental.Identifiable needs to have their Identifier correctly set.
	Update(mctx Context, object elemental.Identifiable) error

	// Delete deletes one or multiple elemental.Identifiables.
	// In order to be deletable, the elemental.Identifiable needs to have their Identifier correctly set.
	Delete(mctx Context, object elemental.Identifiable) error

	// DeleteMany deletes all objects of with the given identity or
	// all the ones matching the filter in the given context.
	DeleteMany(mctx Context, identity elemental.Identity) error

	// Count returns the number of objects with the given identity.
	Count(mctx Context, identity elemental.Identity) (int, error)
}
```

As an example, our processor will need to retrieve all the pizzas, so we'll use the method  `RetrieveMany`.

#### Update our processor

Using the above configuration, we can  instantiate a mongo manipulator, and pass it as a to the processor:

```go
package main

import (
	"context"

	"go.aporeto.io/addedeffect/lombric"
	"go.aporeto.io/backend-demo/models"
	"go.aporeto.io/backend-demo/backend/pizza/internal/processors"
	"go.aporeto.io/bahamut"
	"go.aporeto.io/elemental"
	"go.aporeto.io/manipulate/manipmongo"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var conf = struct {
	Listen      string `mapstructure:"listen"       desc:"Listen address"           default:"127.0.0.1:8000"`
	MongoURL    string `mapstructure:"mongo-url"    desc:"Mongo database address"   default:"mongodb://127.0.0.1:27017"`
	MongoDBName string `mapstructure:"mongo-db"     desc:"Mongo database name"      default:"pizza"`
}{}

func main() {

	// Initialize a context
	ctx, cancel := context.WithCancel(context.Background())
	bahamut.InstallSIGINTHandler(cancel)

	// Initialize configuration
	lombric.Initialize(&conf)

	// Initialize logger
	logger, _ := zap.NewDevelopment()
	zap.ReplaceGlobals(logger.WithOptions(zap.IncreaseLevel(zapcore.DebugLevel)))

	// Initialize mongodb
	mongoManipulator, err := manipmongo.New(conf.MongoURL, conf.MongoDBName)
	if err != nil {
		panic("unable to connect to mongo" + err.Error())
	}

	// Initialize the server
	server := bahamut.New(
		bahamut.OptServiceInfo("pizza", "1.0", nil),
		bahamut.OptModel(map[int]elemental.ModelManager{0: models.Manager(), 1: models.Manager()}),
		bahamut.OptRestServer(conf.Listen),
	)

	// Register the processor of pizzas for the Pizza resource.
	bahamut.RegisterProcessorOrDie(server, processors.NewPizzaProcessor(mongoManipulator), models.PizzaIdentity)

	// Run the server
	server.Run(ctx)

	<-ctx.Done()
}

```

Finally, update the processor `pizzas.go` to keep a reference of that manipulator and use it when needed.

```go
package processors

import (
	"fmt"
	"net/http"

	"go.aporeto.io/backend-demo/models"
	"go.aporeto.io/bahamut"
	"go.aporeto.io/elemental"
	"go.aporeto.io/manipulate"
	"go.uber.org/zap"
)

// PizzaProcessor processes requests coming on /pizzas endpoint
type PizzaProcessor struct {
	mongoManipulator manipulate.TransactionalManipulator
}

// NewPizzaProcessor creates an instance of the Pizza processor.
func NewPizzaProcessor(mongoManipulator manipulate.TransactionalManipulator) *PizzaProcessor {

	return &PizzaProcessor{
		mongoManipulator: mongoManipulator,
	}
}

// ProcessRetrieveMany implements the retrieve-many capability as a bahamut Processor.
func (p *PizzaProcessor) ProcessRetrieveMany(bctx bahamut.Context) error {

	zap.L().Info("received request", zap.String("op", string(bctx.Request().Operation)))

	out := models.PizzasList{}
	if err := p.mongoManipulator.RetrieveMany(nil, &out); err != nil {
		return elemental.NewError("Error", fmt.Sprintf("unable to retrieve pizzas: %s", err.Error()), "pizza", http.StatusInternalServerError)
	}

	bctx.SetOutputData(out)

	return nil
}

```

That's cool, but we don't have any pizza! Let's create one.

#### Create a pizza

So far, our API only allows GET /pizzas which always returns an empty array because we don't have any data:

```
curl http://127.0.0.1:8000/pizzas
[]
```

We want to be able to create a pizza, and so we must implement a POST /pizzas.

* Update your `root.spec` to modify the relations:

```yaml
# Relations
relations:
- rest_name: pizza
  get:
    description: List all pizza available.
  create:
      description: Create a new pizza.
```

* Implement the method `processCreate` in the `pizzas` processor:

```go
// ProcessCreate implements the create capability as a bahamut Processor.
func (p *PizzaProcessor) ProcessCreate(bctx bahamut.Context) error {

	pizza := bctx.InputData().(*models.Pizza)
	return p.mongoManipulator.Create(nil, pizza)
}
```

* Restart your server... and see that you can create a pizza!

```bash
curl -X POST http://127.0.0.1:8000/pizzas \
  -H 'Content-Type: application/json' \
  -d '{ "name": "Meat Lover" }'

curl http://127.0.0.1:8000/pizzas
[{"ID":"5efd02f56eb576c1c7353bf2","name":"Meat Lover"}]
```

> Note: using the same technique, you can implement the `processUpdate` and `processDelete`to support PUT /pizzas and DELETE /pizzas apis.





# Bahamut tutorial - PART 3

The processor is where we want to have our business logic. However, everything that is related to the validation of the object should remain in the model. When the object arrives in the processor, it has already passed all the validations.



In this part, we'll quickly improve our model to add some validations on the pizza object. Some validation can be done directly thanks to the specs. For mode advanced validations, we can use the `$validation` attribute of the model or the attribute itself.



In the example below, we'll make sure the name of the pizza is a required attribute. Then, we'll add an extra validation to avoid having the character `!` in the name:



## Specification validations

* Update your `pizza.spec` to mark the attribute `name` as required

```yaml
# Attributes
attributes:
  v1:
  - name: name
    description: The name of the pizza.
    type: string
    exposed: true
    stored: true
    required: true
    example_value: calzone
```

* Save the file, restart your server
* Try to create a pizza with an empty should return an error

```bash
curl -X POST http://127.0.0.1:8000/pizzas \
  -H 'Content-Type: application/json' \
  -d '{ "name": "" }'
[{"code":422,"data":{"attribute":"name"},"description":"Attribute 'name' is required","subject":"elemental","title":"Validation Error","trace":"unknown"}]
```



## Custom validations

A specification has various attributes that automatically adds validation. For example: `type`, `subtype`, `read_only`, `allowed_values`, etc.

If you can't find the validation you want, you can always use what is called "custom validations" that can be set on both the model and attribute. Let's assume we don't want our pizza name to contain `!`:

* Update your `pizzas.spec` to register a new validation

```yaml
# Attributes
attributes:
  v1:
  - name: name
    description: The name of the pizza.
    type: string
    exposed: true
    stored: true
    required: true
    example_value: calzone
    validations:
    - $validatePizzaName
```

* Create a file called `_validation.mapping` in the `specs` folder that has the following content

```yaml
$validatePizzaName:
  elemental:
    name: ValidatePizzaName
```

* Create a `custom.validation.go` in the `models` folder to do your implementation:

```go
package models

import (
	"net/http"
	"strings"

	"go.aporeto.io/elemental"
)

// makeValidationError returns an elemental error for the validation.
func makeValidationError(attribute string, message string) elemental.Error {
	err := elemental.NewError(
		"Validation Error",
		message,
		"models",
		http.StatusUnprocessableEntity,
	)

	if attribute != "" {
		err.Data = map[string]interface{}{"attribute": attribute}
	}

	return err
}

// ValidatePizzaName validates the pizza name.
func ValidatePizzaName(attribute string, value string) error {
	if strings.Contains(value, "!") {
		return makeValidationError(attribute, "pizza name cannot contain the character '!'")
	}
	return nil
}
```

* Restart the server and observe the following result

```bash
curl -X POST http://127.0.0.1:8000/pizzas \
  -H 'Content-Type: application/json' \
  -d '{ "name": "calzone!" }'
[{"code":422,"data":{"attribute":"name"},"description":"pizza name cannot contain the character '!'","subject":"models","title":"Validation Error","trace":"unknown"}]
```



# Bahamut tutorial - PART 4

In this new section, we are going to create a second micro service to deal with the orders. This service will manage the API `/orders`.

## Creating a new micro-service

It is pretty similar to the pizza service:

* Create an `order.spec` models. Add some validations if needed.

```yaml
# Model
model:
  rest_name: order
  resource_name: orders
  entity_name: Order
  package: core
  group: core
  description: This is an order.
  get:
    description: Retrieve a order given its identifier.
  update:
    description: Update a order given its identifier.
  delete:
    description: Delete a orderge given its identifier.
  extends:
  - '@identifiable'
  - '@timeable'
  validations:
  - $validateOrder

# Attributes
attributes:
  v1:
  - name: items
    description: List of items of the order.
    type: refList
    exposed: true
    subtype: orderitem
    stored: true
    extensions:
      refMode: pointer
```

* This model uses a "detached model" called `orderitem`. It represents an item that can be ordered but does not have any API endpoint.

```yaml
# Model
model:
  rest_name: orderitem
  resource_name: orderitems
  entity_name: OrderItem
  package: core
  group: core
  description: This represents the quantity of a pizza to order.
  extends:
  - '@identifiable'
  detached: true

# Attributes
attributes:
  v1:
  - name: itemType
    description: Type of item. For now, we're only doing pizza.
    type: enum
    exposed: true
    stored: true
    required: true
    allowed_choices:
    - Pizza
    default_value: Pizza

  - name: quantity
    description: Quantity of item to order.
    type: integer
    exposed: true
    stored: true
    required: true
    default_value: 0
```

* Attach the `order` model to the `root` in the `root.spec`:

```yaml
# Relations
relations:
- rest_name: order
  get:
    description: List all orders available.
  create:
    description: Create a new order.
```

* Create the service `order` . Everything is similar to `pizza` except the port used `8001` and the mongo database name.
* Build the service and run it

```bash
go build ./services/order && ./order
2020-07-06T18:53:03.876-0700    INFO    bahamut/rest_server.go:246      API server started      {"address": "127.0.0.1:8001"}
```

* Create an order and list all orders

```bash
curl -X POST http://127.0.0.1:8001/orders \
  -H 'Content-Type: application/json' \
  -d '{ "items": [{"ID": "5efd038c6eb576c1c7353bf3", "itemType": "Pizza", "quantity": 1}] }'
```

```bash
curl http://127.0.0.1:8001/orders
[
  {
    "ID": "5f03d3946eb5763bd85e7eb2",
    "items": [
      {
        "ID": "5efd038c6eb576c1c7353bf3",
        "itemType": "Pizza",
        "quantity": 1
      }
    ]
  }
]
```



## Adding an API Gateway

As we want our customers to have a single endpoint to access both services, we are going to use an API Gateway. Each service will register itself to the gateway which will manage the incoming requests and dispatch them to the correct service.

### Architecture

We are going to add a gateway thanks to bahamut:

1. Each service will register itself to the gateway at startup.
2. Once registered, the gateway can accept requests on a route and redirect the call to the corresponding service

The registration is made thanks to NATS server. The gateway registers to the events received. Each service sends a message to NATS to indicate it is up and running.

![gateway architecture](/Users/chrserafin/Documents/gateway-architecture.png)

### NATS

Check out the [installation guide of NATS server](https://docs.nats.io/nats-server/installation). As I am developing on Mac OS, I am simply installing it via brew:

```bash
brew install nats-server
```

Then, I can run NATS using:

```
nats-server
```

> Note: We haven't secure anything yet... but don't worry, this will come in a next section ;)

### Bahamut Gateway

Now that our NATS server runs perfectly, we can create a new folder "service/gateway" which can hold our service `gateway`. The gateway is listening to requests on port `3443`and is subscribing to the NATS topic `gw.services`:

```go
package main

import (
	"context"

	"go.aporeto.io/addedeffect/lombric"
	"go.aporeto.io/bahamut"
	"go.aporeto.io/bahamut/gateway"
	"go.aporeto.io/bahamut/gateway/upstreamer/push"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var conf = struct {
	Listen string `mapstructure:"listen" desc:"Listen address"  default:":3443""`
	NATS   string `mapstructure:"nats-url" desc:"URL of nats"   default:"nats://127.0.0.1:4222""`
}{}

func main() {

	// Initialize a context
	ctx, cancel := context.WithCancel(context.Background())
	bahamut.InstallSIGINTHandler(cancel)

	// Initialize configuration
	lombric.Initialize(&conf)

	// Initialize logger
	logger, _ := zap.NewDevelopment()
	zap.ReplaceGlobals(logger.WithOptions(zap.IncreaseLevel(zapcore.DebugLevel)))

	// Connect to nats
	pubsub := bahamut.NewNATSPubSubClient(conf.NATS)
	if err := pubsub.Connect(ctx); err != nil {
		panic("unable to connect to nats" + err.Error())
	}

	// Initialize the upstreamer
	upstreamer := push.NewUpstreamer(pubsub, "gw.services")
	<-upstreamer.Start(ctx)

	// Initialize and start the gateway
	gw, err := gateway.New(
		conf.Listen,
		upstreamer,
		gateway.OptionUpstreamURLScheme("http"),
	)
	if err != nil {
		panic(err)
	}
	gw.Start(ctx)

	zap.L().Info("Gateway started", zap.String("listen", conf.Listen))

	<-ctx.Done()
}
```

### Service registration

Both pizza and order services can now register when they are starting and stopping. In the `main.go`, we need to connect to NATS, and notify the gateway on start and stop. This is done with the following code:

```go
var conf = struct {
	Listen      string `mapstructure:"listen"       desc:"Listen address"           default:"127.0.0.1:8000"`
	MongoURL    string `mapstructure:"mongo-url"    desc:"Mongo database address"   default:"mongodb://127.0.0.1:27017"`
	MongoDBName string `mapstructure:"mongo-db"     desc:"Mongo database name"      default:"pizza"`
	NATS        string `mapstructure:"nats-url"     desc:"URL of nats"              default:"nats://127.0.0.1:4222"`
}{}

func main() {
  // ...

  // Connect to nats
	pubsub := bahamut.NewNATSPubSubClient(conf.NATS)
	if err := pubsub.Connect(ctx); err != nil {
		panic("unable to connect to nats" + err.Error())
	}

	// Create the gateway notifier
	pn := push.NewNotifier(pubsub, "gw.services", "pizza", conf.Listen)

	// Initialize the server
	server := bahamut.New(
		bahamut.OptServiceInfo("pizza", "1.0", nil),
		bahamut.OptModel(map[int]elemental.ModelManager{0: models.Manager(), 1: models.Manager()}),
		bahamut.OptRestServer(conf.Listen),
		bahamut.OptPostStartHook(pn.MakeStartHook(ctx, 5*time.Second)),
		bahamut.OptPreStopHook(pn.MakeStopHook()),
	)

	// ...
}
```

### Running everything together

As usual, we need to build and run each service. Use a terminal for each of the command below:

```bash
mongod --dbpath .data/mongo --bind_ip 0.0.0.0
```

```
nats-server
```

```bash
go build ./services/gateway && ./gateway
```

```bash
go build ./services/pizza && ./pizza
```

```bash
go build ./services/order && ./order
```

and check that the gateway can now redirect any registered route:

```bash
curl http://127.0.0.1:3443/pizzas
[
  {
    "ID": "5efd038c6eb576c1c7353bf3",
    "name": "Margarita"
  },
  {
    "ID": "5efe62846eb576d69ace9e83",
    "name": "Calzone"
  }
]
```

Now that we have a clear picture, it's time to refactor a bit to make it clean, and secure our example!


### Next Chapters

Even though the code has been updated until the last part, we still need to document what we did in the following sections:

* Part 5 - Protecting your services with TLS
* Part 6 - Refactoring to avoid code duplication
* Part 7 - Docker deployment / Kubernetes charts